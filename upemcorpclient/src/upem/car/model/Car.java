package upem.car.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Car implements Serializable, ICar {

	class Comment  implements Serializable {
		public Comment(String custId, Date date, String comment, int score) {
			this.custId = custId;
			this.date = date;
			this.comment = comment != null ? comment : "";
			this.score = score;
		}
		@Override
		public
		String toString() {
			return new StringBuilder().append("\n\nClient:").append(custId)
					.append("\tScore:") .append(score)
					.append("\tComment:").append(comment).append("\tDate:").append(date).append("\n").toString();
					
		}
		String custId;
		Date date;
		String comment;
		int score;
	}
	
	private Date inServiceDate;
	private String brand;
	private int category;
	private String color;
	private String licensePlate;
	private int state;
	private boolean alreadyRented;
	private int score;
	private List<Comment> comments= new ArrayList<>();

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#isAlreadyRented()
	 */
	@Override
	public boolean isAlreadyRented() {
		return alreadyRented;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setAlreadyRented(boolean)
	 */
	@Override
	public void setAlreadyRented(boolean alreadyRented) {
		this.alreadyRented = alreadyRented;
	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getState()
	 */
	@Override
	public int getState() {
		return state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setState(int)
	 */
	@Override
	public void setState(int state) {
		Objects.nonNull(State.fromId(state));
		this.state = state;
	}

	public Car() {

	}

	public Car(Date inServiceDate, String brand, int category, String color, String licensePlate, int state) {
		super();
		this.inServiceDate = inServiceDate;
		this.brand = brand;
		Objects.requireNonNull(Category.fromId(category));
		this.category = category;
		this.color = color;
		this.licensePlate = licensePlate;
		this.state = state;
		this.alreadyRented = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getLicensePlate()
	 */
	@Override
	public String getLicensePlate() {
		return licensePlate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setLicensePlate(java.lang.String)
	 */
	@Override
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getInServiceDate()
	 */
	@Override
	public Date getInServiceDate() {
		return inServiceDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setInServiceDate(java.util.Date)
	 */
	@Override
	public void setInServiceDate(Date inServiceDate) {
		this.inServiceDate = inServiceDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getBrand()
	 */
	@Override
	public String getBrand() {
		return brand;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setBrand(java.lang.String)
	 */
	@Override
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getCategory()
	 */
	@Override
	public int getCategory() {
		return category;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setCategory(int)
	 */
	@Override
	public void setCategory(int category) {
		Objects.requireNonNull(Category.fromId(category));
		this.category = category;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getColor()
	 */
	@Override
	public String getColor() {
		return color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setColor(java.lang.String)
	 */
	@Override
	public void setColor(String color) {
		this.color = color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#getScore()
	 */
	@Override
	public int getScore() {
		return score;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.ICar#setScore(int)
	 */
	@Override
	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("Plaque:")
				.append(this.licensePlate)
				.append(" - Marque:").append(this.brand)
				.append("- Catégorie:").append(Category.fromId(this.category) )
				.append("\n.Commentaires:")
				.append(comments)
				.toString();

	}

	@Override
	public void addComment(String custId, int score, String comment, Date returnedDate) {
		comments.add(new Comment(custId, returnedDate,  comment , score));
		
	}

	

}
