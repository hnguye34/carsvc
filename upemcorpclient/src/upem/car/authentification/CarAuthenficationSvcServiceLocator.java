/**
 * CarAuthenficationSvcServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package upem.car.authentification;

public class CarAuthenficationSvcServiceLocator extends org.apache.axis.client.Service implements upem.car.authentification.CarAuthenficationSvcService {

    public CarAuthenficationSvcServiceLocator() {
    }


    public CarAuthenficationSvcServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CarAuthenficationSvcServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CarAuthenficationSvc
    private java.lang.String CarAuthenficationSvc_address = "http://localhost:8080/upemcar/services/CarAuthenficationSvc";

    public java.lang.String getCarAuthenficationSvcAddress() {
        return CarAuthenficationSvc_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CarAuthenficationSvcWSDDServiceName = "CarAuthenficationSvc";

    public java.lang.String getCarAuthenficationSvcWSDDServiceName() {
        return CarAuthenficationSvcWSDDServiceName;
    }

    public void setCarAuthenficationSvcWSDDServiceName(java.lang.String name) {
        CarAuthenficationSvcWSDDServiceName = name;
    }

    public upem.car.authentification.CarAuthenficationSvc getCarAuthenficationSvc() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CarAuthenficationSvc_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCarAuthenficationSvc(endpoint);
    }

    public upem.car.authentification.CarAuthenficationSvc getCarAuthenficationSvc(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            upem.car.authentification.CarAuthenficationSvcSoapBindingStub _stub = new upem.car.authentification.CarAuthenficationSvcSoapBindingStub(portAddress, this);
            _stub.setPortName(getCarAuthenficationSvcWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCarAuthenficationSvcEndpointAddress(java.lang.String address) {
        CarAuthenficationSvc_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (upem.car.authentification.CarAuthenficationSvc.class.isAssignableFrom(serviceEndpointInterface)) {
                upem.car.authentification.CarAuthenficationSvcSoapBindingStub _stub = new upem.car.authentification.CarAuthenficationSvcSoapBindingStub(new java.net.URL(CarAuthenficationSvc_address), this);
                _stub.setPortName(getCarAuthenficationSvcWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CarAuthenficationSvc".equals(inputPortName)) {
            return getCarAuthenficationSvc();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://authentification.car.upem", "CarAuthenficationSvcService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://authentification.car.upem", "CarAuthenficationSvc"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CarAuthenficationSvc".equals(portName)) {
            setCarAuthenficationSvcEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
