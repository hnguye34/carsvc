/**
 * CarAuthenficationSvc.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package upem.car.authentification;

public interface CarAuthenficationSvc extends java.rmi.Remote {
    public boolean logIn(upem.car.authentification.Customer user) throws java.rmi.RemoteException;
    public boolean signUp(upem.car.authentification.Customer user) throws java.rmi.RemoteException;
}
