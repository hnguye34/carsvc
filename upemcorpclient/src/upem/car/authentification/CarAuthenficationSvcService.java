/**
 * CarAuthenficationSvcService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package upem.car.authentification;

public interface CarAuthenficationSvcService extends javax.xml.rpc.Service {
    public java.lang.String getCarAuthenficationSvcAddress();

    public upem.car.authentification.CarAuthenficationSvc getCarAuthenficationSvc() throws javax.xml.rpc.ServiceException;

    public upem.car.authentification.CarAuthenficationSvc getCarAuthenficationSvc(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
