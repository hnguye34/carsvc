package upem.car.authentification;

public class CarAuthenficationSvcProxy implements upem.car.authentification.CarAuthenficationSvc {
  private String _endpoint = null;
  private upem.car.authentification.CarAuthenficationSvc carAuthenficationSvc = null;
  
  public CarAuthenficationSvcProxy() {
    _initCarAuthenficationSvcProxy();
  }
  
  public CarAuthenficationSvcProxy(String endpoint) {
    _endpoint = endpoint;
    _initCarAuthenficationSvcProxy();
  }
  
  private void _initCarAuthenficationSvcProxy() {
    try {
      carAuthenficationSvc = (new upem.car.authentification.CarAuthenficationSvcServiceLocator()).getCarAuthenficationSvc();
      if (carAuthenficationSvc != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)carAuthenficationSvc)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)carAuthenficationSvc)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (carAuthenficationSvc != null)
      ((javax.xml.rpc.Stub)carAuthenficationSvc)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public upem.car.authentification.CarAuthenficationSvc getCarAuthenficationSvc() {
    if (carAuthenficationSvc == null)
      _initCarAuthenficationSvcProxy();
    return carAuthenficationSvc;
  }
  
  public boolean logIn(upem.car.authentification.Customer user) throws java.rmi.RemoteException{
    if (carAuthenficationSvc == null)
      _initCarAuthenficationSvcProxy();
    return carAuthenficationSvc.logIn(user);
  }
  
  public boolean signUp(upem.car.authentification.Customer user) throws java.rmi.RemoteException{
    if (carAuthenficationSvc == null)
      _initCarAuthenficationSvcProxy();
    return carAuthenficationSvc.signUp(user);
  }
  
  
}