package upem.corps.rentcar.svr;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.rpc.ServiceException;

import upem.car.model.Booking;
import upem.car.model.Car;
import upem.car.model.IBooking;
import upem.car.model.ICar;
import upem.car.model.ICarRentSvc;
import upem.car.model.IUpemClient;
import upem.corps.authentification.svc.IUserManager;

public class CarRentalClient extends UnicastRemoteObject implements IUpemClient {

	private ICarRentSvc carRenSvc;
	private String userId;
	private String pass;
	private UUID session;

	public CarRentalClient() throws RemoteException, MalformedURLException, NotBoundException {
		carRenSvc = (ICarRentSvc) Naming.lookup("rmi://localhost/UpemRentalSvc");

	}

	private void connectToSvc(String args, String args2) throws RemoteException {

		try {
			userId = args;
			pass = args2;
			session = carRenSvc.connect(this);

			System.out.println("---> Vous �tes connect� au service de location");
		} catch (Exception e) {

			System.out.println(e.getMessage());
			System.out.println("----->Connexion impossible:" + userId + "/" + pass);
		}
	}

	public static void main(String[] args) throws Exception {

		String codebase = "file:/C:/Nam/carsvc";
		System.setProperty("java.rmi.server.codebase", codebase);
		System.setProperty("java.security.policy", "C:/Nam/carsvc/upemcorpclient/src/sec.policy");

		CarRentalClient client = new CarRentalClient();
		client.connectToSvc(args[0], args[1]);

		client.menu();

	}

	private int menu() throws Exception {
		int result = 1;

		try (Scanner scanner = new Scanner(System.in)) {
			printMenu();
			while (scanner.hasNextLine()) {
				int choice = Integer.parseInt(scanner.nextLine().toUpperCase());

				switch (choice) {
				case 0:
					this.connectToSvc(userId, pass);
					break;

				case 1:
					System.out.print("Depuis (YYYYMMDD):");

					if (scanner.hasNextLine()) {
						String yyyyMMdd = scanner.nextLine();

						SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
						Date fromDate = (Date) formatter.parse(yyyyMMdd);

						System.out.print("To (YYYYMMDD):");

						if (scanner.hasNextLine()) {
							String to = scanner.nextLine();
							Date toDate = (Date) formatter.parse(to);

							List<ICar> cars = availableCar(fromDate, toDate);
						}
					}

					break;
				case 2:
					Scenario1();
				case 3:
					System.out.print("Plaque d'immatriculation:");
					if (scanner.hasNextLine()) {
						String carPlate = scanner.nextLine();
						this.bookACar(carPlate);
					}
					break;
				case 4:
					System.out.print("N� r�servation:");
					if (scanner.hasNextLine()) {
						int bookingId = Integer.parseInt(scanner.nextLine().toUpperCase());
						this.returnACar(bookingId);
					}
					break;
				case 5:
					System.out.print("N� r�servation:");
					if (scanner.hasNextLine()) {
						int bookingId = Integer.parseInt(scanner.nextLine().toUpperCase());
						System.out.print("Score:");
						if (scanner.hasNextLine()) {
							int score = Integer.parseInt(scanner.nextLine().toUpperCase());
							if (scanner.hasNextLine()) {
								String comment = scanner.nextLine().toUpperCase();
								this.scoreACar(bookingId, comment, score);
							}
						}
					}
					break;
				case 6:
					System.out.print("Depuis (YYYYMMDD):");

					if (scanner.hasNextLine()) {
						String yyyyMMdd = scanner.nextLine();

						SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
						Date fromDate = (Date) formatter.parse(yyyyMMdd);

						List<IBooking> bookings = this.carRenSvc.getBooking(this.session, fromDate);
						StringBuilder st = new StringBuilder();
						for (IBooking b : bookings) {

							st.append(b).append("\n");
						}

						System.out.println(st.toString());
					}

					break;
				default:
					break;
				}
				printMenu();
			}

		}

		return result;
	}

	private void printMenu() {
		System.out.println("\t\t\t*** LOCATION VEHICULE   ***\n");
		System.out.println("\t\t\t 0: se reconnecter \n");
		System.out.println("\t\t\t 1: liste v�hcules disponibles \n");
		System.out.println("\t\t\t 2: sc�nario de location/retour/notation v�hicule.\n");
		System.out.println("\t\t\t 3: louer un v�hicule.\n");
		System.out.println("\t\t\t 4: rendre un v�hicule.\n");
		System.out.println("\t\t\t 5: noter un v�hicule.\n");
		System.out.println("\t\t\t 6: Lister les r�sevations.\n");
		System.out.print("Votre choix:");
	}

	private void Scenario1() throws Exception {
		List<ICar> cars = availableCar(Calendar.getInstance().getTime(), Calendar.getInstance().getTime());

		String plate = "BV-123-NM";
		// historyOfCar(plate);

		System.out.print("Choix de voiture:");

		try (Scanner scanner = new Scanner(System.in);) {

			if (scanner.hasNextLine()) {
				String line = scanner.nextLine().toUpperCase();

				Booking booking = bookACar(line);
				historyOfCar(line);

				if (booking != null) {
					booking = returnACar(booking.getId());
				}

				if (booking != null) {
					System.out.println("\n\nV�hicule  rendu: " + booking);
					scoreACar(booking.getId(), "Voiture nickek", 3);
				}
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private ICar historyOfCar(String plate) {
		ICar car = null;
		try {

			car = carRenSvc.getCarWithAllScores(session, plate);

			System.out.println("\n\nDescription compl�te: \n\n" + car);

		} catch (Exception e) {
			System.out.println("\n\n Erreur notation v�hicule: " + e.getMessage());
		}
		return car;
	}

	private void scoreACar(int bookingId, String comment, int score) {
		try {

			Car car = (Car) carRenSvc.scoring(session, bookingId, score, comment);
			car = (Car) carRenSvc.getCarWithAllScores(session, car.getLicensePlate());

			System.out.println("\n\nV�hicule  not�: " + car);
		} catch (Exception e) {
			System.out.println("\n\n Erreur notation v�hicule: " + e.getMessage());
		}

	}

	private Booking returnACar(int bookingId) {
		try {
			System.out.println("\n\n On rend : " + String.valueOf(bookingId));
			Booking booking = (Booking) carRenSvc.returnCar(session, bookingId);

			return booking;
		} catch (Exception e) {
			System.out.println("\n\n Erreur retour v�hicule: " + e.getMessage());
		}
		return null;

	}

	private Booking bookACar(String plate) throws Exception {
		Date from = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(from); // Now use today date.
		c.add(Calendar.DATE, 3); // Adding 5 days

		Date to = c.getTime();

		try {
			Booking booking = (Booking) carRenSvc.book(session, plate, from, to);

			System.out.println("\n\nR�servation faite: " + booking);

			return booking;
		} catch (Exception e) {
			System.out.println("\n\nR�servation erreur: " + e.getMessage());
		}

		return null;
	}

	private List<ICar> availableCar(Date fromDate, Date toDate) throws RemoteException {

		System.out.println("-----> availableCar");
		List<ICar> carsCateg1 = carRenSvc.getAvailableCars(fromDate, toDate, 1);
		System.out.println(carsCateg1.size());
		List<ICar> carsCateg2 = carRenSvc.getAvailableCars(fromDate, toDate, 2);
		System.out.println(carsCateg2.size());
		List<ICar> cars = Stream.concat(carsCateg1.stream(), carsCateg2.stream()).collect(Collectors.toList());

		System.out.println("\n\n" + String.format("\n\nNombre:\t%d\n", cars.size()) + " voitures disponibles entre: "
				+ fromDate.toLocaleString() + " et " + toDate.toLocaleString());
		System.out.println("\n");
		for (ICar car : cars) {
			StringBuilder st = new StringBuilder();

			st.append(car.getLicensePlate()).append("\t").append(ICar.Category.fromId(car.getCategory())).append("\t")
					.append(car.getBrand()).append("\t").append(car.getColor()).append("\n");

			System.out.println(st.toString());
		}

		return cars;
	}

	@Override
	public void notifierAvailableCar(String car) throws RemoteException {

		System.out.println("OK. Notification recue:" + car + " est disponible !!!");

	}

	@Override
	public String getPass() {
		return this.pass;
	}

	@Override
	public String getUser() {
		return this.userId;
	}

}
