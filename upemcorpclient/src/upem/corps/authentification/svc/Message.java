package upem.corps.authentification.svc;

public class Message {

	String destinationId;
	String msg;

	public Message(String destinationId, String msg) {

		this.destinationId = destinationId;
		this.msg = msg;
	}

}
