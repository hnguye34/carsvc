
import java.rmi.*;

public interface IUpemCarAuthenfication  extends Remote {
    String SVC_UPEM_CAR_ADDR = "rmi://localhost:2001/UpemCarAuthentificationSvc";
	boolean existUser (User user )  throws RemoteException;
	boolean createUser ( User user )  throws RemoteException;
}
