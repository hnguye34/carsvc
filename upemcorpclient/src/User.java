
import java.io.Serializable;



public class User  implements Serializable {
	public enum Profil { UPEM_CORPS, PUBLIC};
	
	private  String id;
	private  String email;
	private  String firstName;
	private  String lastName;
	private  String passwd;
	private Profil profil;

	public User(String id, String email, String firstName, String lastName, Profil profil, String passwd) {
		
		this.id = id;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.profil = profil;
		this.passwd = passwd;
		
	}
	public String getId() {
		return id;
	}
	
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	public Profil getProfil() {
		return profil;
	}
	public void setProfil(Profil profil) {
		this.profil = profil;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

}
