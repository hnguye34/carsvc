package upem.corps.rentcar.svr;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import upem.car.dao.BookingDao;
import upem.car.dao.CarDao;
import upem.car.dao.DbManager;
import upem.car.model.Car;
import upem.car.model.IBooking;
import upem.car.model.Booking;
import upem.car.model.ICar;
import upem.car.model.ICarRentSvc;
import upem.car.model.IUpemClient;

import upem.car.util.Util;
import upem.corps.authentification.svc.IUserManager;

public class CarRentSvr extends UnicastRemoteObject implements ICarRentSvc {

	class SessionInfo {

		private static final long TIME_OUT_MN = 1;

		public SessionInfo(IUpemClient client, String userId) {
			this.client = client;
			this.userId = userId;
		}

		IUpemClient client;
		String userId;
		long lastRequestTime = System.currentTimeMillis();

		synchronized public void setLastActionTime(long currentTime) {
			lastRequestTime = currentTime;

		}

		public boolean isTimeOut(long currentTime) {
			long duration = TimeUnit.MILLISECONDS.toMinutes(currentTime - this.lastRequestTime);
			return duration > TIME_OUT_MN;
		}

	}

	public class UserNotifierSrv {

		private static final int INACTIVE_TIME_LIMIT = 1000; // ms
		private static final int LIMIT_QUEUE_SIZE = 50;
		private final Thread notifierThread;
		private final Thread checkInactiveSessionThread;
		private final BlockingQueue<String> queue = new LinkedBlockingQueue<>(LIMIT_QUEUE_SIZE);

		public UserNotifierSrv() {
			notifierThread = new Thread(() -> {
				readMessage();
			});
			checkInactiveSessionThread = new Thread(() -> {
				checkInactiveSession();
			});

		}

		/*
		 * 
		 * 
		 */
		private void checkInactiveSession() {

			while (!Thread.currentThread().isInterrupted()) {
				try {

					Iterator<Entry<UUID, SessionInfo>> sessionIt = sessions.entrySet().iterator();

					while (sessionIt.hasNext()) {
						String idUser = "";
						try {
							Entry<UUID, SessionInfo> aSession = sessionIt.next();

							SessionInfo sessionInfo = aSession.getValue();
							idUser = sessionInfo.userId;
							sessionInfo.client.getUser();

							if (sessionInfo.isTimeOut(System.currentTimeMillis())) {
								System.out.println("----> Utilisateur d�passe le timeout, donc �ject�:" + idUser
										+ sessionInfo.lastRequestTime);
								sessionIt.remove();
							}
						} catch (Exception e) {
							System.out.println(e);
							System.out.println("----> Utilisateur sera d�connect�:" + idUser);
							sessionIt.remove();
						}
					}

					Thread.sleep(INACTIVE_TIME_LIMIT);

				} catch (Exception e) {
					System.out.println(e);
				}

			}
		}

		public void putMessage(String carPlate) throws InterruptedException {
			queue.put(carPlate);

		}

		private void readMessage() {

			while (!Thread.currentThread().isInterrupted()) {
				try {
					notifier(queue.take());
				} catch (InterruptedException e) {
					break;
				} catch (Exception e) {
					System.out.println(e);
				}

			}

		}

		public void start() {
			this.notifierThread.start();
			this.checkInactiveSessionThread.start();
		}

		public void stop() {
			this.notifierThread.interrupt();
			this.checkInactiveSessionThread.interrupt();
		}

	}

	final UserNotifierSrv notificationServer = new UserNotifierSrv();
	ConcurrentHashMap<String, Car> cars = new ConcurrentHashMap<>();
	ConcurrentHashMap<UUID, SessionInfo> sessions = new ConcurrentHashMap<>();
	ConcurrentHashMap<String, List<SessionInfo>> waitingUsers = new ConcurrentHashMap<>();
	private final IUserManager upemCorpUserManager = getUserManager();
	private final DbManager ds = new DbManager();

	public CarRentSvr() throws MalformedURLException, RemoteException, NotBoundException {

		Objects.requireNonNull(upemCorpUserManager);
		Objects.requireNonNull(ds);
		notificationServer.start();
	}

	@Override
	public List<ICar> getAvailableCars(Date from, Date to, int category) throws RemoteException {

		try {
			to = Util.setLastHr(to);
			from = Util.setFirstHr(from);
			List<ICar> cars = CarDao.getAvailableCars(ds, from, to, category);
			return cars;

		} catch (Exception e) {

		}
		return null;
	}

	private UUID generateSessionId() {
		return java.util.UUID.randomUUID();
	}

	@Override
	public UUID connect(IUpemClient client) throws RemoteException, SQLException {

		String userId = client.getUser();
		String pass = client.getPass();
		if (!upemCorpUserManager.checkUser(userId, pass))
			throw new IllegalArgumentException(userId);

		UUID uiid = generateSessionId();

		if (sessions.containsKey(uiid))
			throw new IllegalArgumentException("Cas impossible");

		sessions.put(uiid, new SessionInfo(client, userId));

		return uiid;
	}

	private SessionInfo checkUser(UUID sessionId) {

		SessionInfo session = sessions.get(sessionId);

		if (session == null)
			throw new IllegalArgumentException("SessionId inexistante" + sessionId);

		session.setLastActionTime(System.currentTimeMillis());

		return session;
	}

	@Override
	public List<IBooking> getBooking(UUID sessionId, Date from) throws RemoteException {
		// TODO Auto-generated method stub

		SessionInfo session = checkUser(sessionId);
		List<IBooking> bookings = new ArrayList<>();
		try {
			bookings = BookingDao.getAllOfCustomer(ds, session.userId, from);
		} catch (Exception e) {
			System.out.println(e);
		}
		return bookings;
	}

	@Override
	public IBooking book(UUID sessionId, String carPlate, Date from, Date to) throws Exception {

		SessionInfo session = checkUser(sessionId);

		to = Util.setLastHr(to);
		from = Util.setFirstHr(from);
		long diff = to.getTime() - from.getTime();
		long diffDay = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

		if (diffDay < 1)
			throw new IllegalStateException(String.format("Impossible de r�sever moins d'un jour.", carPlate));

		diff = Util.getToday().getTime() - from.getTime();
		diffDay = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		if (diff >= 1)
			throw new IllegalStateException(String.format("Impossible de r�sever dans le pass�:%s,  %s, %d", carPlate,
					from.toLocaleString(), diff));

		if (!CarDao.isAvailableCar(ds, carPlate, from, to)) {

			List<SessionInfo> users = this.waitingUsers.get(carPlate);

			if (users == null) {
				users = new ArrayList<>();
				users.add(session);
				this.waitingUsers.put(carPlate, users);
			} else
				users.add(session);

			throw new IllegalStateException(
					String.format("Cette voiture '%s' n'est plus disponible � cette periode (%s , %s )", carPlate,
							from.toLocaleString(), to.toLocaleString()));

		}
		IBooking booking = new Booking(carPlate, session.userId, from, to, 0, "EUR", Booking.State.PAID);

		IBooking newBooking = BookingDao.insert(ds, booking);

		return newBooking;

	}

	@Override
	public IBooking returnCar(UUID sessionId, int bookingId) throws Exception {
		SessionInfo sessionInfo = checkUser(sessionId);
		String custId = sessionInfo.userId;

		try (java.sql.Connection con = ds.getConnection()) {
			boolean isAutoCommit = con.getAutoCommit();
			con.setAutoCommit(false);
			try {
				IBooking booking = BookingDao.getABookingOfCustomerById(con, bookingId);

				if (booking != null) {
					String carPlate = booking.getCarId();
					if (!booking.getCustomerId().equals(custId))
						throw new IllegalStateException(
								"Vous ne pouvez pas rendre un v�hicule que vous n'avez pas r�sev� par vous-m�me:"
										+ booking.getCarId());

					if (booking.isState(Booking.State.CAR_RETURNED))
						throw new IllegalStateException(
								"Ce v�hicule" + carPlate + " est d�j� rendu:" + String.valueOf(bookingId));

					IBooking newBooking = BookingDao.updateState(con, bookingId, IBooking.State.CAR_RETURNED);

					if (newBooking == null)
						throw new IllegalStateException("Probl�me de mise � jour de cetteervation r�s" + bookingId);

					this.notificationServer.putMessage(carPlate);
					return newBooking;

				} else
					throw new IllegalStateException("Vous n'avez pas lou� ce v�hicule." + bookingId);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				con.rollback();
				throw e;
			} finally {
				con.setAutoCommit(isAutoCommit);
			}
		}

	}

	@Override
	public ICar scoring(UUID sessionId, int bookingId, int score, String comment) throws RemoteException, SQLException {
		SessionInfo sessionInfo = checkUser(sessionId);
		try (java.sql.Connection con = ds.getConnection()) {
			IBooking booking = BookingDao.getABookingOfCustomerById(con, bookingId);

			String carPlate;
			if (booking != null) {
				String custId = sessionInfo.userId;
				if (!booking.getCustomerId().equals(custId))
					throw new IllegalStateException(
							"Vous ne pouvez pas noter une r�servation qui ne vous n'appartient pas :"
									+ booking.getCarId());

				carPlate = booking.getCarId();
				if (booking.isScored())
					throw new IllegalStateException("Vous avez d�j� not� ce v�hicule:" + booking.getCarId());

				if (!booking.isState(Booking.State.CAR_RETURNED))
					throw new IllegalStateException("Vous ne pouvez pas noter un v�hicule non rendu:" + carPlate);

				boolean isAutoCommit = con.getAutoCommit();
				con.setAutoCommit(false);
				try {
					if (comment == null)
						comment = new String();

					IBooking newBooking = BookingDao.setIsScored(booking, con, comment, score);
					Objects.requireNonNull(newBooking);
					ICar car = CarDao.addScore(carPlate, score, con, comment);

					Objects.requireNonNull(car);
					con.commit();
					return car;
				} catch (Exception e) {
					con.rollback();
					throw e;

				} finally {
					con.setAutoCommit(isAutoCommit);
				}

			} else
				throw new IllegalStateException(
						"Vous n'avez pas lou� ce v�hicule � cette date:" + String.valueOf(bookingId));
		}
	}

	@Override
	public ICar getCarWithAllScores(UUID sessionId, String carPlate) throws RemoteException, SQLException {
		checkUser(sessionId);

		ICar car = CarDao.getCar(carPlate, this.ds.getConnection());

		if (car == null)
			throw new IllegalStateException("V�hicule non trouv�:" + carPlate);

		List<Booking> bookings = BookingDao.getBookingCarHistory(this.ds, carPlate);

		bookings.stream().filter(b -> b.getState() == Booking.State.CAR_RETURNED.getId() && b.isScored())
				.forEach(bo -> car.addComment(bo.getCustomerId(), bo.getScore(), bo.getComment(), bo.getReturnedDate())

		);

		return car;
	}

	private IUserManager getUserManager() throws MalformedURLException, RemoteException, NotBoundException {

		String codebase = "file:/C:/Nam/carsvc";
		System.setProperty("java.rmi.server.codebase", codebase);
		System.setProperty("java.security.policy", "C:/Nam/carsvc/upemcorpclient/src/sec.policy");

		IUserManager upemCorpUserManager = (IUserManager) Naming.lookup("rmi://localhost:1098/" + IUserManager.adress);

		Objects.requireNonNull(upemCorpUserManager);
		return upemCorpUserManager;

	}

	public void notifier(String carPlate) throws RemoteException {
		try {

			for (SessionInfo e : this.waitingUsers.get(carPlate))
				e.client.notifierAvailableCar(carPlate);

			// can remove all theses user now
			this.waitingUsers.remove(carPlate);

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
