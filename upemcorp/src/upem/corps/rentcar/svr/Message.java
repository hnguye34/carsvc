package upem.corps.rentcar.svr;

import java.util.UUID;

public class Message {

	UUID destinationId;
	String  carPlate;

	public Message(UUID destinationId, String carPlate) {

		this.destinationId = destinationId;
		this.carPlate = carPlate;
	}

}
