package upem.corps.rentcar.svr;

import java.rmi.registry.Registry;

import upem.car.model.ICarRentSvc;

public class CarRentalStarter {
	public static final int port = 1099;
	public static final String name = "UpemRentalSvc";
	public static String stringUrlRmi = "rmi://localhost:" + String.valueOf(port);

	public static void main(String args[]) {
		try {
			Registry registry = java.rmi.registry.LocateRegistry.createRegistry(port);
			ICarRentSvc carRentSvc = new CarRentSvr();
			registry.rebind(name, carRentSvc);

			// SQLiteConnectionPoolDataSource ds = new SQLiteConnectionPoolDataSource();
			// ds.setUrl("jdbc:sqlite:C:/Nam/carsvc/carsvc/src/main/resources/upemcar.db");

			// Properties properties = new Properties();
			// String stringFactRmi = "org.sqlite.javax.SQLiteConnectionPoolDataSource";
			// properties.put(Context.INITIAL_CONTEXT_FACTORY,
			// "com.sun.jndi.rmi.registry.RegistryContextFactory");

			// properties.put(Context.PROVIDER_URL, stringUrlRmi);
			//
			// InitialContext context = new InitialContext(properties);
			// context.rebind("jdbc/DB", (DataSource) ds);

			// ds.setUrl(stringUrlRmi);
			System.out.println("Server location v�hicule est pr�t..");
		} catch (Exception e) {
			System.out.println("Trouble: " + e);
		}
	}

}
