package upem.corps.svc.provider;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Objects;

import upem.car.model.ICarRentSvc;
import upem.corps.authentification.svc.IUserManager;

public class SvcProvider {

	static ICarRentSvc getRentSvc (String user, String pass) throws MalformedURLException, RemoteException, NotBoundException, SQLException {
		IUserManager userManager = getUserManager();
		boolean isGood = userManager.checkUser(user, pass);
		
		ICarRentSvc carRentSvc = null;
		if ( isGood )
			carRentSvc  = getRentalSvc();
		
		return carRentSvc;
	}

	private static IUserManager getUserManager() throws MalformedURLException, RemoteException, NotBoundException {
		
		String codebase = "file:/C:/Nam/carsvc";
		System.setProperty("java.rmi.server.codebase", codebase);
		System.setProperty("java.security.policy", "C:/Nam/carsvc/upemcorpclient/src/sec.policy");

		IUserManager upemCorpUserManager = (IUserManager) Naming.lookup("rmi://localhost/" + IUserManager.adress);

		Objects.requireNonNull(upemCorpUserManager);
		return upemCorpUserManager;

	}
	
	private static ICarRentSvc getRentalSvc() throws MalformedURLException, RemoteException, NotBoundException {
		// if (upemCorpUserManager == null) {
		String codebase = "file:/C:/Nam/carsvc";
		System.setProperty("java.rmi.server.codebase", codebase);
		System.setProperty("java.security.policy", "C:/Nam/carsvc/upemcorpclient/src/sec.policy");

		ICarRentSvc carRentSvc = (ICarRentSvc) Naming.lookup("rmi://localhost:1098/UpemRentalSvc" );

		Objects.requireNonNull(carRentSvc);
		carRentSvc = (ICarRentSvc) Naming.lookup("rmi://localhost/UpemRentalSvc");
		return carRentSvc;

	}
}
