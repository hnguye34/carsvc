package upem.corps.authentification.svc;


import java.rmi.registry.Registry;


public class AuthentificationMain {
	 public static void main(String args[]) {
		 try {
			 Registry registry = java.rmi.registry.LocateRegistry.createRegistry(IUserManager.port);
			 IUserManager userManager = new AuthentificationSrv();
			 registry.bind(IUserManager.adress, userManager);
			 System.out.println("Svc Authenfication d'UpemCorps lanc�..");
		 }
		 catch (Exception e) {
		 System.out.println("Trouble: " + e);
		 }

}
}
