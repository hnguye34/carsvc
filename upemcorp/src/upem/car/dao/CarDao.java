package upem.car.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import upem.car.model.Booking;
import upem.car.model.Car;
import upem.car.model.IBooking;
import upem.car.model.ICar;
import upem.car.util.Util;

public class CarDao {

	public static List<ICar> getAvailableCars(DbManager ds, Date from, Date to, int category) throws SQLException {

		from = Util.setFirstHr(from);
		to = Util.setLastHr(to);
		String sql = String.format(
				"select InServiceDate, Brand, Category, Color, Plate, State, Score  from CAR c  where c.State = 1 and c.Category =%d and not exists  (select null from BOOKING b where b.State <> 3 and  c.Plate=b.CarPlate  and ( (  b.FromDate >= %d and b.FromDate <= %d ) or (  b.ToDate >= %d and b.ToDate <= %d )) ) ",
				category, from.getTime(), to.getTime(), from.getTime(), to.getTime());
		try (java.sql.Connection con = ds.getConnection(); Statement stmt = con.createStatement();) {

			try (ResultSet rs = stmt.executeQuery(sql)) {

				List<ICar> cars = new ArrayList<>();

				while (rs.next()) {
					Car car = new Car(new Date(rs.getLong("InServiceDate")), rs.getString("Brand"),
							rs.getInt("Category"), rs.getString("Color"), rs.getString("Plate"), rs.getInt("State"));
					car.setScore(rs.getInt("Score"));
					cars.add(car);
				}

				return cars;
			}
		}

	}

	public static boolean isAvailableCar(DbManager ds, String plate, Date from, Date to) throws SQLException {

		from = Util.setFirstHr(from);
		to = Util.setLastHr(to);

		String sql = String.format(
				"select null  from CAR c  where c.Plate ='%s' and not exists (select null from BOOKING b where  b.State =%d and c.Plate=b.CarPlate  and ( (  b.FromDate >= %d and b.FromDate <= %d ) or (  b.ToDate >= %d and b.ToDate <= %d )) )  LIMIT 1",
				plate, IBooking.State.PAID.getId(), from.getTime(), to.getTime(), from.getTime(), to.getTime());

		try (java.sql.Connection con = ds.getConnection();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {

			boolean exists = rs.next();

			return exists;
		}

	}

	public static ICar addScore(String carPlate, int score, Connection con, String comment) throws SQLException {
		String sqlUpdateCar = String.format("update CAR set Score = Score + %d where Plate='%s'", score, carPlate);

		try (Statement stmt = con.createStatement()) {
			int insCount = stmt.executeUpdate(sqlUpdateCar);

			if (insCount != 1)
				throw new IllegalStateException("Impossible metttre a jour le score");

			ICar car = getCar(carPlate, con);

			return car;

		} catch (Exception e) {
			con.rollback();
			throw e;
		}

	}

	/*
	 * public static Car getCar(String plateCar) throws SQLException {
	 * 
	 * if (plateCar.isEmpty()) return null;
	 * 
	 * plateCar = plateCar.toUpperCase();
	 * 
	 * String sql = String.format(
	 * " select InServiceDate, Brand, Category, Color, Plate, State, Score  from CAR   where upper(Plate) ='%s' "
	 * , plateCar);
	 * 
	 * try (java.sql.Connection con = DbManager.getConnection(); Statement stmt =
	 * con.createStatement();) {
	 * 
	 * ResultSet rs = stmt.executeQuery(sql);
	 * 
	 * if (rs.next()) { Car car = new Car(new Date(rs.getLong("InServiceDate")),
	 * rs.getString("Brand"), rs.getInt("Category"), rs.getString("Color"),
	 * rs.getString("Plate"), rs.getInt("State")); car.setScore(rs.getInt("Score"));
	 * return car; }
	 * 
	 * return null; } }
	 */

	public static ICar getCar(String carPlate, java.sql.Connection con) throws SQLException {

		if (carPlate.isEmpty())
			throw new IllegalArgumentException("Vehicule does not exist");

		String sql = String.format(
				" select InServiceDate, Brand, Category, Color, Plate, State, Score  from CAR   where upper(Plate) ='%s' ",
				carPlate.toUpperCase());

		try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {

			if (rs.next()) {
				ICar car = new Car(new Date(rs.getLong("InServiceDate")), rs.getString("Brand"), rs.getInt("Category"),
						rs.getString("Color"), rs.getString("Plate"), rs.getInt("State"));
				car.setScore(rs.getInt("Score"));
				return car;
			} else
				throw new IllegalArgumentException("Vehicule does not exist:" + carPlate);

		}
	}

}
