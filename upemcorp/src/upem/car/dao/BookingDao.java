package upem.car.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import upem.car.model.IBooking;

import upem.car.util.Util;
import upem.car.model.*;

public class BookingDao {

	static public IBooking insert(DbManager dbManager, IBooking booking) throws Exception {

		Date from = Util.setFirstHr(booking.getFrom());
		Date to = Util.setLastHr(booking.getTo());
		String sql = String.format(
				"insert into BOOKING (CarPlate, CustId, FromDate, ToDate, Amount, Ccy, State, IsScored) values ('%s','%s',%d, %d, %s, '%s', %d, 0) ",
				booking.getCarId(), booking.getCustomerId(), from.getTime(), to.getTime(),
				String.format(Locale.US, "%.2f", booking.getAmount()), booking.getCcy(), booking.getState());

		try (java.sql.Connection con = dbManager.getConnection()) {
			boolean isAutoComit = con.getAutoCommit();
			con.setAutoCommit(false);
			try {
				;
				Statement stmt = con.createStatement();
				int insCount = stmt.executeUpdate(sql);

				if (insCount != 1)
					throw new java.lang.Exception("Cannot insert booking.");

				String custId = booking.getCustomerId();
				booking = BookingDao.getABookingOfCustomer(con, custId, from, booking.getCarId());

				if (booking == null)
					throw new IllegalStateException("Impossible d'ins�rer la r�servation.");

				con.commit();

			} catch (Exception e) {
				con.rollback();
			} finally {
				con.setAutoCommit(isAutoComit);

			}
		}
		return booking;
	}

	static public List<IBooking> getAllOfCustomer(DbManager dbManager, String custId, Date from) throws Exception {

		List<IBooking> bookings = new ArrayList<>();

		String sql = String.format(
				"select BookingId, CarPlate, CustId, FromDate, ToDate, Amount, Ccy, State, IsScored, ReturnedDate, Score, Comment from BOOKING where CustId='%s' and FromDate >=%d",
				custId, from.getTime());
		try (java.sql.Connection con = dbManager.getConnection(); Statement stmt = con.createStatement();ResultSet rs = stmt.executeQuery(sql)) {

			

			while (rs.next()) {
				IBooking booking = new upem.car.model.Booking(rs.getInt("BookingId"), rs.getString("CarPlate"),
						rs.getString("CustId"), new Date(rs.getLong("FromDate")), new Date(rs.getLong("ToDate")),
						rs.getFloat("Amount"), rs.getString("Ccy"), rs.getInt("State"));

				booking.setiScored(rs.getInt("IsScored"));

				booking.setScore(rs.getInt("Score"));
				booking.setComment(rs.getString("Comment"));
				if (rs.getLong("ReturnedDate") != 0)
					booking.setReturnedDate(new Date(rs.getLong("ReturnedDate")));

				bookings.add(booking);

			}

			return bookings;
		}

	}

	public static List<Booking> getByCar(DbManager dbManager, String carPlate, Date from) throws SQLException {
		List<Booking> bookings = new ArrayList<>();

		Statement stmt = null;
		String sql = String.format(
				"select BookingId, CarPlate, CustId, FromDate, ToDate, Amount, Ccy, State, IsScored, ReturnedDate, Score, Comment from BOOKING where CarPlate='%s' and  FromDate =%d",
				carPlate, from.getTime());

		try {
			stmt = dbManager.getConnection().createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Booking booking = new Booking(rs.getInt("BookingId"), rs.getString("CarPlate"), rs.getString("CustId"),
						new Date(rs.getLong("FromDate")), new Date(rs.getLong("ToDate")), rs.getFloat("Amount"),
						rs.getString("Ccy"), rs.getInt("State"));

				booking.setiScored(rs.getInt("IsScored"));
				booking.setScore(rs.getInt("Score"));
				booking.setComment(rs.getString("Comment"));
				if (rs.getLong("ReturnedDate") != 0)
					booking.setReturnedDate(new Date(rs.getLong("ReturnedDate")));

				bookings.add(booking);
			}
			return bookings;
		} finally {
			if (stmt != null) {
				stmt.close();
			}

		}
	}

	public static IBooking getABookingOfCustomer(Connection con, String custId, Date from, String carPlate)
			throws SQLException {

		Booking booking = null;
		from = Util.setFirstHr(from);
		String sql = String.format(
				"select BookingId, CarPlate, CustId, FromDate, ToDate, Amount, Ccy, State, IsScored, ReturnedDate, Score, Comment from BOOKING where CustId='%s' and CarPlate='%s' and  FromDate =%d",
				custId, carPlate, from.getTime());

		try (Statement stmt = con.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				booking = new Booking(rs.getInt("BookingId"), rs.getString("CarPlate"), rs.getString("CustId"),
						new Date(rs.getLong("FromDate")), new Date(rs.getLong("ToDate")), rs.getFloat("Amount"),
						rs.getString("Ccy"), rs.getInt("State"));

				booking.setiScored(rs.getInt("IsScored"));
				booking.setScore(rs.getInt("Score"));
				booking.setComment(rs.getString("Comment"));
				if (rs.getLong("ReturnedDate") != 0)
					booking.setReturnedDate(new Date(rs.getLong("ReturnedDate")));

			}

			return booking;

		}

	}

	public static IBooking getABookingOfCustomerById(Connection con, int bookingId) throws SQLException {

		String sql = String.format(
				"select BookingId, CarPlate, CustId, FromDate, ToDate, Amount, Ccy, State, IsScored, ReturnedDate, Score, Comment from BOOKING where BookingId =%d",
				bookingId);

		try (Statement stmt = con.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
			IBooking booking = null;
			if (rs.next()) {

				booking = new Booking(rs.getInt("BookingId"), rs.getString("CarPlate"), rs.getString("CustId"),
						new Date(rs.getLong("FromDate")), new Date(rs.getLong("ToDate")), rs.getFloat("Amount"),
						rs.getString("Ccy"), rs.getInt("State"));
				booking.setiScored(rs.getInt("IsScored"));
				booking.setScore(rs.getInt("Score"));
				booking.setComment(rs.getString("Comment"));
				if (rs.getLong("ReturnedDate") != 0)
					booking.setReturnedDate(new Date(rs.getLong("ReturnedDate")));

				if (rs.next())
					throw new IllegalStateException("Pluseur r�servation pour le m�me id:" + String.valueOf(bookingId));

			}

			return booking;
		}
	}

	public static boolean custRented(DbManager dbManager, String custId, String carPlate, Date bookingDate)
			throws SQLException {

		String sql = String.format(
				"select null from BOOKING where CustId='%s' and CarPlate='%s' and    FromDate =%d LIMIT 1", custId,
				carPlate);

		try (java.sql.Connection con = dbManager.getConnection(); Statement stmt = con.createStatement();) {

			ResultSet rs = stmt.executeQuery(sql);
			boolean exist = rs.next();
			return exist;
		}

	}

	public static IBooking updateState(Connection con, int bookingId, Booking.State finished)
			throws SQLException, CarSvcErr {

		String sql = String.format("update  BOOKING set State = %d, ReturnedDate=%d where BookingId =%d",
				finished.getId(), new Date().getTime(), bookingId);

		try (Statement stmt = con.createStatement();) {

			int rs = stmt.executeUpdate(sql);

			if (rs != 1) {
				con.rollback();
				throw new CarSvcErr("Plusieurs r�servations correspondant cet id:" + String.valueOf(bookingId));
			}
			IBooking newBooking = null;
			if (rs == 1)
				newBooking = getABookingOfCustomerById(con, bookingId);

			return newBooking;

		}
	}

	public static IBooking setIsScored(IBooking booking, Connection connection, String comment, int score)
			throws SQLException {

		String sql = String.format("update  BOOKING set IsScored = %d, Score=%d, Comment='%s' where BookingId=%d ", 1,
				score, comment, booking.getId());

		try (Statement stmt = connection.createStatement();) {

			int rs = stmt.executeUpdate(sql);

			if (rs != 1) {
				throw new IllegalStateException("Plusieurs r�servations pour ce v�hicule:" + booking.getCarId()
						+ " - Client:" + booking.getCustomerId());
			}

			IBooking newBooking = getABookingOfCustomerById(connection, booking.getId());
			Objects.requireNonNull(newBooking);

			return newBooking;

		}

	}

	public static List<Booking> getBookingCarHistory(DbManager dbManager, String carPlate) throws SQLException {
		List<Booking> bookings = new ArrayList<>();
		String sql = String.format(
				"select BookingId, CarPlate, CustId, FromDate, ToDate, Amount, Ccy, State, IsScored, ReturnedDate, Score, Comment from BOOKING where CarPlate='%s' order by ReturnedDate desc",
				carPlate);
System.out.println("---->getBookingCarHistory");
		try(Connection con =dbManager.getConnection() ; Statement stmt = con.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Booking booking = new Booking(rs.getInt("BookingId"), rs.getString("CarPlate"), rs.getString("CustId"),
						new Date(rs.getLong("FromDate")), new Date(rs.getLong("ToDate")), rs.getFloat("Amount"),
						rs.getString("Ccy"), rs.getInt("State"));
				System.out.println("---->getBookingCarHistory boucle");
				booking.setiScored(rs.getInt("IsScored"));
				booking.setScore(rs.getInt("Score"));
				booking.setComment(rs.getString("Comment"));
				if ( rs.getLong("ReturnedDate") != 0)
					booking.setReturnedDate(new Date(rs.getLong("ReturnedDate")));
				
				bookings.add(booking);
			}
			return bookings;
		} 

	}
}
