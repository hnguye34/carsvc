package upem.car.model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;



public interface ICarRentSvc extends Remote {

	UUID connect(IUpemClient client) throws RemoteException, SQLException;

	List<IBooking> getBooking(UUID sessionId, Date from) throws RemoteException;

	List<ICar> getAvailableCars(java.util.Date from, java.util.Date to, int category) throws RemoteException;

	IBooking book(UUID sessionId, String carPlate, Date from, Date to)
			throws RemoteException,  Exception;

	IBooking returnCar(UUID sessionId, int bookingId) throws RemoteException, SQLException, Exception;

	ICar scoring(UUID sessionId, int bookingId, int score, String comment) throws RemoteException, SQLException;
	ICar getCarWithAllScores(UUID sessionId, String carPlate) throws RemoteException, SQLException;

}
