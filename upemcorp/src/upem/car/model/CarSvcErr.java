package upem.car.model;

public class CarSvcErr extends Exception {
	
	public  CarSvcErr() {
        super("Cette voiture n'est plus disponible. Vous serez avertie par email lorsqu'elle est disponible pour louer ");
    }

	public  CarSvcErr( String cause) {
		super(cause);
    }

}
