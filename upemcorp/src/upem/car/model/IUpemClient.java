package upem.car.model;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IUpemClient extends  Remote {
	void notifierAvailableCar(String carPlate)  throws RemoteException; ;
	String getPass() throws RemoteException;
	String getUser() throws RemoteException;
}
