package upem.car.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Booking implements Serializable, IBooking {

	private int id;

	public void setScored(boolean isScored) {
		this.isScored = isScored;
	}

	private String carId;
	private String customerId;
	private Date from;
	private Date to;
	private double amount;
	private String ccy;
	private int state;
	private boolean isScored = false;
	private Date returnedDate = getMaxDate();
	private int score = 0;
	private String comment = "";
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setAmount(double)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setAmount(double)
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Booking() {

	}

	public Booking(int id, String carId, String customerId, Date from, Date to, float amount, String ccy, int state) {
		this.id = id;
		this.carId = carId;
		this.customerId = customerId;
		this.from = from;
		this.to = to;
		this.amount = amount;
		this.ccy = ccy;
		Objects.requireNonNull(State.fromId(state));
		this.setState(state);
		this.isScored = false;
	}

	public Booking(String carId, String customerId, Date from, Date to, float amount, String ccy, int state) {

		this.carId = carId;
		this.customerId = customerId;
		this.from = from;
		this.to = to;
		this.amount = amount;
		this.ccy = ccy;
		Objects.requireNonNull(State.fromId(state));
		this.setState(state);
		this.isScored = false;
	}

	public Booking(String carId, String customerId, Date from, Date to, double amount, String ccy, State state) {

		this.carId = carId;
		this.customerId = customerId;
		this.from = from;
		this.to = to;
		this.amount = amount;
		this.ccy = ccy;
		Objects.requireNonNull(state);
		this.setState(state.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getCarId()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getCarId()
	 */
	@Override
	public String getCarId() {
		return carId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setCarId(java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setCarId(java.lang.String)
	 */
	@Override
	public void setCarId(String carId) {
		this.carId = carId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getCustomerId()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getCustomerId()
	 */
	@Override
	public String getCustomerId() {
		return customerId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setCustomerId(java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setCustomerId(java.lang.String)
	 */
	@Override
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getFrom()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getFrom()
	 */
	@Override
	public Date getFrom() {
		return from;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setFrom(java.util.Date)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setFrom(java.util.Date)
	 */
	@Override
	public void setFrom(Date from) {
		this.from = from;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getTo()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getTo()
	 */
	@Override
	public Date getTo() {
		return to;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setTo(java.util.Date)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setTo(java.util.Date)
	 */
	@Override
	public void setTo(Date to) {
		this.to = to;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getAmount()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getAmount()
	 */
	@Override
	public double getAmount() {
		return amount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setAmount(float)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setAmount(float)
	 */
	@Override
	public void setAmount(float amount) {
		this.amount = amount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getCcy()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getCcy()
	 */
	@Override
	public String getCcy() {
		return ccy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setCcy(java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setCcy(java.lang.String)
	 */
	@Override
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getState()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#getState()
	 */
	@Override
	public int getState() {
		return state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setState(int)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setState(int)
	 */
	@Override
	public void setState(int state) {
		this.state = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#isScored()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#isScored()
	 */
	@Override
	public boolean isScored() {
		return isScored;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setiScored(int)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#setiScored(int)
	 */
	@Override
	public void setiScored(int isScored) {

		this.isScored = isScored == 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#isState(upem.car.model.Booking.State)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBooking#isState(upem.car.model.Booking.State)
	 */
	@Override
	public boolean isState(State state) {
		boolean result = this.state == state.getId();
		return result;
	}

	public Date getReturnedDate() {
		return returnedDate;
	}

	public void setReturnedDate(Date returnedDate) {
		this.returnedDate = returnedDate;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("N�: ").append(id).append("\tPlaque:").append(this.carId).append("\tClient :").append(this.customerId)
				.append("\tDe: ").append(this.from.toLocaleString()).append("\tA: ")
				.append(this.from.toLocaleString()).append("\tNote: ").append(this.isScored).append("\tScore:")
				.append(this.score).append("\tCommentaire: ").append(this.comment).append("\tRetour le: ")
				.append(this.returnedDate).append("\n").toString();

	}

	private static Date getMaxDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2099);
	//	calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
}
