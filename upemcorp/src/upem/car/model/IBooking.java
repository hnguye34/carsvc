package upem.car.model;

import java.util.Date;

public interface IBooking {

	public enum State {
		WAITING(1), PAID(2), CAR_RETURNED(3);
		private int id;

		State(int id) {
			this.id = id;
		}

		public static State fromId(int id) {
			for (State type : values()) {
				if (type.id == id) {
					return type;
				}
			}
			throw new IllegalArgumentException();
		}

		public int getId() {
			return id;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setAmount(double)
	 */
	void setAmount(double amount);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getCarId()
	 */
	String getCarId();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setCarId(java.lang.String)
	 */
	void setCarId(String carId);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getCustomerId()
	 */
	String getCustomerId();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setCustomerId(java.lang.String)
	 */
	void setCustomerId(String customerId);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getFrom()
	 */
	Date getFrom();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setFrom(java.util.Date)
	 */
	void setFrom(Date from);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getTo()
	 */
	Date getTo();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setTo(java.util.Date)
	 */
	void setTo(Date to);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getAmount()
	 */
	double getAmount();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setAmount(float)
	 */
	void setAmount(float amount);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getCcy()
	 */
	String getCcy();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setCcy(java.lang.String)
	 */
	void setCcy(String ccy);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#getState()
	 */
	int getState();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setState(int)
	 */
	void setState(int state);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#isScored()
	 */
	boolean isScored();

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#setiScored(int)
	 */
	void setiScored(int isScored);

	/*
	 * (non-Javadoc)
	 * 
	 * @see upem.car.model.IBook#isState(upem.car.model.Booking.State)
	 */
	boolean isState(State state);

	int getId();

	void setId(int id);
	
	public Date getReturnedDate() ;
	public void setReturnedDate(Date returnedDate) ;
	public int getScore();
	public void setScore(int score) ;
	public String getComment() ;
	public void setComment(String comment);

}