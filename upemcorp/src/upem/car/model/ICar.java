package upem.car.model;

import java.util.Date;

public interface ICar {
	public enum Category {
		SEDAN(1), MONOSPACE(2);

		private int id;

		Category(int id) {
			this.id = id;
		}

		public static Category fromId(int id) {
			for (Category type : values()) {
				if (type.id == id) {
					return type;
				}
			}
			throw new IllegalArgumentException();
		}

	}

	public enum State {
		IN_SERVICE(1), SOLD(2);

		private int id;

		State(int id) {
			this.id = id;
		}

		public static State fromId(int id) {
			for (State type : values()) {
				if (type.id == id) {
					return type;
				}
			}
			throw new IllegalArgumentException();
		}

	}

	boolean isAlreadyRented();

	void setAlreadyRented(boolean alreadyRented);

	int getState();

	void setState(int state);

	String getLicensePlate();

	void setLicensePlate(String licensePlate);

	Date getInServiceDate();

	void setInServiceDate(Date inServiceDate);

	String getBrand();

	void setBrand(String brand);

	int getCategory();

	void setCategory(int category);

	String getColor();

	void setColor(String color);

	int getScore();

	void setScore(int score);

	void addComment(String custId, int score, String comment, Date returnedDate);

}