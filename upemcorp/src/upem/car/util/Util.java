package upem.car.util;


import java.util.Calendar;
import java.util.Date;




public class Util {
	public final static int FIRST_LAST_HOUR = 9;
	public final static int LAST_WORK_HOUR = 19;
	public static final String CCY_BY_DEFAULT = "EUR";
	public static Date setLastHr (Date date)  {
		Calendar calendar = Calendar.getInstance();

		
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, LAST_WORK_HOUR);
		calendar.set(Calendar.MINUTE, 00);
	    calendar.set(Calendar.SECOND, 00);
	    calendar.set(Calendar.MILLISECOND, 00);
	    
		return calendar.getTime();
	}

	public static Date setFirstHr(Date from) {
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(from);
		calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
	
	public static Date getToday() {
		Calendar calendar = Calendar.getInstance();
		
		calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
	
	public static Date getNow() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}
	
	public static Date getMaxDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2099);
		calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
	
	
	

	
}
