package upem.car.authentification;

import java.rmi.*;
import java.sql.SQLException;

public interface ICarAuthenficationSvc  extends Remote {
	String SVC_UPEM_CAR_ADDR = "UpemCarAuthentificationSvc";
	int PORT = 2001;
	boolean logIn (String custId, String passwd, int profil )  throws RemoteException, SQLException;
	boolean signUp ( Customer user )  throws RemoteException;
}
