package upem.car.authentification;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.sql.SQLException;
import java.util.Objects;

import upem.car.authentification.Customer.Profil;
import upem.car.dao.CustomerDao;
import upem.corps.authentification.svc.IUserManager;
import upem.corps.authentification.svc.UpemUser;

public class CarAuthenficationSvc implements ICarAuthenficationSvc {

	private IUserManager upemCorpUserManager;

	public CarAuthenficationSvc() {
	}

	private IUserManager getUpemConnection() throws MalformedURLException, RemoteException, NotBoundException {
		if (upemCorpUserManager == null) {
			String codebase = "file:/C:/Nam/carsvc";
			System.setProperty("java.rmi.server.codebase", codebase);
			System.setProperty("java.security.policy", "C:/Nam/carsvc/upemcorpclient/src/sec.policy");

			upemCorpUserManager = (IUserManager) Naming.lookup("rmi://localhost:1098/" + IUserManager.adress);

			Objects.requireNonNull(upemCorpUserManager);
			return upemCorpUserManager;
		}
		
		return upemCorpUserManager; 
	}

	@Override
	public boolean signUp(Customer user) throws RemoteException {
		try {
			if (Profil.fromId(user.getProfil()) == Profil.UPEM_CORPS)
				importUserFromUpemCorp(user.getId(), user.getPasswd());
			return true;

		} catch (Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}

	@Override
	public boolean logIn(String custId, String passwd, int profil) throws RemoteException, SQLException {
		try {
			if (Profil.fromId(profil) == Profil.UPEM_CORPS)
				importUserFromUpemCorp(custId, passwd);
			else {
				return CustomerDao.exist(custId, passwd, profil);
			}

			return true;
		} catch (Exception e) {
			throw new IllegalArgumentException("Authentification impossible pour " + custId + ":" + e.getMessage());
		}
	}

	private void importUserFromUpemCorp(String id, String passwd)
			throws RemoteException, InterruptedException, MalformedURLException, NotBoundException, SQLException {

		
		boolean res = getUpemConnection().checkUser(id, passwd);
		if (!res )
			throw new IllegalStateException("Client Umpem Corps non authentifie:" + id);

	}
	
	public void asynNotifierUser(String id ) throws RemoteException, InterruptedException, MalformedURLException, NotBoundException {
		
	}
}
