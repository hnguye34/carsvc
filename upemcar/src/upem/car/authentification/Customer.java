package upem.car.authentification;

import java.io.Serializable;
public class Customer  implements Serializable {
	public  enum Profil { UPEM_CORPS ( 1), PUBLIC (2);
			private int id;
			Profil(int id) { this.id = id ; }
			
			 public static Profil fromId(int id) {
	                for (Profil type : values()) {
	                    if (type.id == id) {
	                        return type;
	                    }
	                }
	                throw new IllegalArgumentException();
	            }

	};
	
	private  String id;
	private  String email;
	private  String firstName;
	private  String lastName;
	private  String passwd;
	private Profil profil;

	public Customer()
	{
		
	}
	
	public Customer(String id, String email, String firstName, String lastName, int profil, String passwd) {
	
		this.id = id;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	//	this.profil = profil == 1 ? Profil.;
		this.passwd = passwd;
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	public int getProfil() {
		return profil.id;
	}
	
	/*public Profil getProfil() {
		return profil;
	}*/
	

	
	public void setProfil(int profil) {
		this.profil= Profil.fromId(profil);
	}
	
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

}
