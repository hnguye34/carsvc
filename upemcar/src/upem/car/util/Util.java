package upem.car.util;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.rpc.ServiceException;

import org.tempuri.ConverterLocator;

public class Util {
	public final static int FIRST_LAST_HOUR = 9;
	public final static int LAST_WORK_HOUR = 19;
	public static final String CCY_BY_DEFAULT = "EUR";

	public static Date setLastHr(Date date) {
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, LAST_WORK_HOUR);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND, 00);

		return calendar.getTime();
	}

	public static Date setFirstHr(Date from) {
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(from);
		calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getToday() {
		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getNow() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}

	public static Date getMaxDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2099);
		calendar.set(Calendar.HOUR_OF_DAY, FIRST_LAST_HOUR);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static double getXRate(String to) throws RemoteException, ServiceException {

		to = to.toUpperCase();

		if (to.length() != 3)
			throw new IllegalArgumentException("currency does not exist.");
		if (to.equals(CCY_BY_DEFAULT))
			return 1;

		Calendar c = Calendar.getInstance();

		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		BigDecimal rate = new ConverterLocator().getConverterSoap().getConversionRate(CCY_BY_DEFAULT, to.toUpperCase(),
				c);

		if (rate.doubleValue() == 0)
			throw new IllegalArgumentException("Exchange rate does not exist between:" + CCY_BY_DEFAULT + " and " + to);
		return rate.doubleValue();

	}

}
