package upem.car.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Date;


import upem.car.svc.common.Car;

import upem.car.svc.sale.SaleCar;
import upem.car.svc.sale.ShoppingCart;


public class ShoppingCartDao {

	private static ShoppingCart getCustCart(String custId, Connection connection) {

		if (custId.isEmpty())
			return null;

		String sql = String.format("select CustId, Ccy  from CART   where lower(CustId) ='%s' limit 1",
				custId.toLowerCase());

		try (Statement stmt = connection.createStatement();) {

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				ShoppingCart cart = new ShoppingCart(rs.getString("CustId"), rs.getString("Ccy"));
				return cart;
			}

			return null;
		} catch (Exception e) {
			return null;
		}
	}

	public static ShoppingCart setCcy(ShoppingCart shoppingCart, String ccy, Connection connection)
			throws SQLException {

		String sqlUpdate = String.format("update CART set Ccy='%s'  where lower(CustId) ='%s' limit 1",
				shoppingCart.getCustId().toLowerCase(), ccy.toUpperCase());

		try (Statement stmt = connection.createStatement();) {

			int count = stmt.executeUpdate(sqlUpdate);

			if (count != 1)
				throw new IllegalStateException(
						"Impossible de modifier la devise du parnier:" + shoppingCart.getCustId());
			
			ShoppingCart cart = ShoppingCartDao.getShoppingCart(shoppingCart.getCustId(), connection);
			return cart;
		}
	}

	public static ShoppingCart createIfAbsentShoppingCart(String custId, Connection connection, String ccy)
			throws Exception {

		if (custId.isEmpty() || ccy.isEmpty())
			return null;

		ShoppingCart cart = getShoppingCart(custId, connection);

		if (cart == null) {
			String sqlCreateShoppingCart = String.format("insert into CART ( CustId,  Ccy) values ('%s','%s')",
					custId, ccy);

			try (Statement stmt = connection.createStatement()) {

				int insCount = stmt.executeUpdate(sqlCreateShoppingCart);

				if (insCount != 1)
					throw new java.lang.Exception("Cannot create the shopping cart for the customer:" + custId);

				cart = new ShoppingCart();
				cart.setCcy(ccy);
				cart.setCustId(custId);
				return cart;
			}
		} else {

			if (!cart.getCcy().equalsIgnoreCase(ccy))
				throw new java.lang.Exception(
						"Error: the currency is no longer the same:" + ccy + " vs" + cart.getCcy());

			return cart;
		}
	}

	public static ShoppingCart getShoppingCart(String custId) throws SQLException {

		if (custId.isEmpty())
			return null;

		try (java.sql.Connection con = DbManager.getConnection(); Statement stmt = con.createStatement();) {

			ShoppingCart shoppingCart = getShoppingCart(custId, con);
			return shoppingCart;
		}
	}

	public static ShoppingCart getShoppingCart(String custId, Connection con) {

		if (custId.isEmpty())
			return null;

		String sql = String.format(
				"select InServiceDate,Brand, Category, Color, Plate, State, Price, Score  from  CARTITEM d, CAR c, SALEPRICE p  where lower(d.CustId) = '%s' and c.Plate = d.CarPlate and p.CarPlate = c.Plate",
				custId.toLowerCase());

		try (Statement stmt = con.createStatement();) {

			ShoppingCart shoppingCart = getCustCart(custId, con);

			if (shoppingCart == null)
				return null;

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Car car = new Car(new Date(rs.getLong("InServiceDate")), rs.getString("Brand"), rs.getInt("Category"),
						rs.getString("Color"), rs.getString("Plate"), rs.getInt("State"));
				car.setScore(rs.getInt("Score"));
				shoppingCart.addCar(new SaleCar(car, rs.getDouble("Price")));
			}

			return shoppingCart;
		} catch (Exception e) {
			return null;
		}
	}

	public static void addCar(ShoppingCart shoppingCart, String carPlate, Connection con) throws Exception {

		try (Statement stmt = con.createStatement()) {

			String sqlInsertCarInCart = String.format("insert into CARTITEM ( CustId, CarPlate) values ('%s','%s')",
					shoppingCart.getCustId(), carPlate);

			int insCount = stmt.executeUpdate(sqlInsertCarInCart);

			if (insCount != 1)
				throw new java.lang.Exception(
						"Cannot add cart in the cart for the user:" + shoppingCart.getCustId() + "/" + carPlate);

		}

	}

	/*
	 * public static ShoppingCart insertShoppingCart(String custId, String ccy,
	 * double xRate, double carPrice) throws Exception { // TODO Auto-generated
	 * method stub String sqlInsertCustCart = String.format(
	 * "insert into CUSTCART ( CustId,  Ccy, UpdateDate, ExchRate) values ('%s','%s',%s, %d, %d)"
	 * , custId, ccy, Util.getNow().getTime(), xRate);
	 * 
	 * try (java.sql.Connection con = DbManager.getConnection(); Statement stmt =
	 * con.createStatement();) {
	 * 
	 * int insCount = stmt.executeUpdate(sqlInsertCustCart);
	 * 
	 * if (insCount != 1) throw new
	 * java.lang.Exception("Cannot create shopping cart for the customer:" +
	 * custId);
	 * 
	 * ShoppingCart cart = getShoppingCart(custId); con.commit();
	 * 
	 * return cart; }
	 * 
	 * }
	 * 
	 * public static ShoppingCart addCar(ShoppingCart cart, Car car, double
	 * priceCar) throws Exception {
	 * 
	 * String sqlInsertCart = String.
	 * format("insert into CART ( CustId, CarPlate, Amount) values ('%s','%s', %d)",
	 * cart.getCustId(), car.getLicensePlate(), priceCar);
	 * 
	 * try (java.sql.Connection con = DbManager.getConnection(); Statement stmt =
	 * con.createStatement();) {
	 * 
	 * int insCount = stmt.executeUpdate(sqlInsertCart);
	 * 
	 * if (insCount != 1) throw new java.lang.Exception("Cannot insert CUSTCART.");
	 * 
	 * insCount = stmt.executeUpdate(sqlInsertCart);
	 * 
	 * if (insCount != 1) throw new java.lang.Exception("Cannot insert CART.");
	 * 
	 * ShoppingCart finalCart = getShoppingCart(cart.getCustId());
	 * 
	 * con.commit();
	 * 
	 * return finalCart; } }
	 */

	public static ShoppingCart removeCar(Connection con, String custId, Car car) throws Exception {

		String sqlDelCart = String.format("delete from CARTITEM where  CustId = '%s' and upper(CarPlate)='%s' ", custId,
				car.getLicensePlate().toUpperCase());

		try ( Statement stmt = con.createStatement();) {

			int insCount = stmt.executeUpdate(sqlDelCart);

			if (insCount != 1)
				throw new IllegalStateException(
						"impossible de retirer le v�hicule" + car.getLicensePlate() + " du panier de " + custId);

			ShoppingCart cart = getShoppingCart(custId, con);

			

			return cart;
		}
	}

	
}
