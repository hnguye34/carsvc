package upem.car.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Date;
import java.util.Locale;

import upem.car.svc.common.Car;
import upem.car.svc.sale.Order;
import upem.car.svc.sale.OrderItem;
import upem.car.svc.sale.SaleCar;
import upem.car.svc.sale.ShoppingCart;

public class OrderDao {

	private static Order getOrder(long orderId, Connection connection) {

		String sql = String.format(
				"select OrderId, CustId, PayDate, Ccy, XRate  from ORDERS   where OrderId =%d limit 1", orderId);

		try (Statement stmt = connection.createStatement();) {

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				Order order = new Order(rs.getLong("OrderId"), rs.getString("CustId"), rs.getString("Ccy"),
						rs.getDouble("XRate"), new Date(rs.getLong("PayDate")));
				return order;
			}

			return null;
		} catch (Exception e) {
			return null;
		}
	}

	public static Order createIfAbsentShoppingCart(Connection connection, String custId, Date payDate, String ccy,
			double xRate) throws Exception {

		if (custId.isEmpty() || ccy.isEmpty())
			return null;

		{
			String sqlCreateOrder = "insert into ORDERS (  CustId, PayDate, Ccy, XRate) values (?, ? ,?, ?)";

			try (PreparedStatement stmt = connection.prepareStatement(sqlCreateOrder,
					Statement.RETURN_GENERATED_KEYS)) {

				stmt.setString(1, custId);
				stmt.setLong(2, payDate.getTime());
				stmt.setString(3, ccy.toUpperCase());
				stmt.setDouble(4, xRate);

				int insCount = stmt.executeUpdate();
				if (insCount != 1) {
					throw new SQLException("Creating order failed, no rows affected.");
				}

				Order order = new Order();
				order.setCcy(ccy);
				order.setCustId(custId);
				order.setPayDate(payDate);
				order.setxRate(xRate);

				try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						order.setOrderId(generatedKeys.getLong(1));

					} else {
						throw new SQLException("Creating order failed, no ID obtained.");
					}
				}

				return order;
			}
		}

	}

	public static OrderItem getOrderItem(Order order, String carPlate, Connection con) throws Exception {

		try (Statement stmt = con.createStatement()) {

			Car car = CarDao.getCar(carPlate, con);
			
			String select = String.format("select OrderId, CarPlate, Amount from ORDERITEM where OrderId= %d and CarPlate='%s'",
					order.getOrderId() , carPlate );

			ResultSet rs = stmt.executeQuery(select);

			if (!rs.next() || car == null)
				throw new java.lang.Exception(
						"Cannot find cart in the cart for the user:" + order.getCustId() + "/" + carPlate);

			OrderItem orderItem = new OrderItem(car, rs.getDouble("Amount"));
			orderItem.setOrderId(rs.getLong("OrderId"));
			
			return orderItem;

		}

	}

	public static void addCar(Order order, String carPlate, double amount, Connection con) throws Exception {

		try (Statement stmt = con.createStatement()) {

			String sqlInsertCarInOrder = String.format(
					"insert into ORDERITEM ( OrderId, CarPlate, Amount) values ('%s','%s', %s)", order.getOrderId(),
					carPlate, String.format(Locale.US, "%.2f", amount));

			int insCount = stmt.executeUpdate(sqlInsertCarInOrder);

			if (insCount != 1)
				throw new java.lang.Exception(
						"Cannot add cart in the cart for the user:" + order.getCustId() + "/" + carPlate);

		}

	}

	/*
	 * public static ShoppingCart insertShoppingCart(String custId, String ccy,
	 * double xRate, double carPrice) throws Exception { // TODO Auto-generated
	 * method stub String sqlInsertCustCart = String.format(
	 * "insert into CUSTCART ( CustId,  Ccy, UpdateDate, ExchRate) values ('%s','%s',%s, %d, %d)"
	 * , custId, ccy, Util.getNow().getTime(), xRate);
	 * 
	 * try (java.sql.Connection con = DbManager.getConnection(); Statement stmt =
	 * con.createStatement();) {
	 * 
	 * int insCount = stmt.executeUpdate(sqlInsertCustCart);
	 * 
	 * if (insCount != 1) throw new
	 * java.lang.Exception("Cannot create shopping cart for the customer:" +
	 * custId);
	 * 
	 * ShoppingCart cart = getShoppingCart(custId); con.commit();
	 * 
	 * return cart; }
	 * 
	 * }
	 * 
	 * public static ShoppingCart addCar(ShoppingCart cart, Car car, double
	 * priceCar) throws Exception {
	 * 
	 * String sqlInsertCart = String.
	 * format("insert into CART ( CustId, CarPlate, Amount) values ('%s','%s', %d)",
	 * cart.getCustId(), car.getLicensePlate(), priceCar);
	 * 
	 * try (java.sql.Connection con = DbManager.getConnection(); Statement stmt =
	 * con.createStatement();) {
	 * 
	 * int insCount = stmt.executeUpdate(sqlInsertCart);
	 * 
	 * if (insCount != 1) throw new java.lang.Exception("Cannot insert CUSTCART.");
	 * 
	 * insCount = stmt.executeUpdate(sqlInsertCart);
	 * 
	 * if (insCount != 1) throw new java.lang.Exception("Cannot insert CART.");
	 * 
	 * ShoppingCart finalCart = getShoppingCart(cart.getCustId());
	 * 
	 * con.commit();
	 * 
	 * return finalCart; } }
	 */

}
