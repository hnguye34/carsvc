package upem.car.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DbManager {

	
	public static Connection getConnection() {   
	    Connection connection = null;   
	    try {
	        Context envCtx = (Context) new InitialContext().lookup("java:comp/env");
	        DataSource ds = (DataSource) envCtx.lookup("jdbc/DB");
	        connection = ds.getConnection();
	        connection.setAutoCommit(true);
	        Objects.requireNonNull(connection);
	    }
	    catch (Exception e) {
	        System.out.println("Connection error: " + e.getMessage());   
	    }
	    return connection; 
	}
	
	 public static Connection connect1() {
	        Connection conn = null;
	        try {
	            // db parameters
	            String url = "jdbc:sqlite:C:/Nam/carsvc/carsvc/src/main/resources/upemcar.db";
	            // create a connection to the database
	            conn = DriverManager.getConnection(url);
	            
	            System.out.println("Connection to SQLite has been established.");
	            return conn;
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        /* finally {
	            try {
	                if (conn != null) {
	                    conn.close();
	                }
	            } catch (SQLException ex) {
	                System.out.println(ex.getMessage());
	            }
*/
			return conn;
	        
	    }
	 
	 	

}
