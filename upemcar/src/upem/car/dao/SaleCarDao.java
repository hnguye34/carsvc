package upem.car.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import upem.car.svc.common.Car;
import upem.car.svc.sale.SaleCar;
import upem.car.svc.sale.ShoppingCart;
import upem.car.util.Util;

public class SaleCarDao {

	public static List<SaleCar> getAvailableCars(Date from, Date to, int category) throws SQLException {

		from = Util.setFirstHr(from);
		to = Util.setLastHr(to);

		String sql = String.format(
				"select c.InServiceDate 'InServiceDate', c.Brand 'Brand', c.Category 'Category', c.Color 'Color', c.Plate 'Plate', c.State 'State', p.Price 'Price' , c.Score 'Score' from CAR c  inner join SALEPRICE p on p.CarPlate = c.Plate  where c.State = 1 and c.Category =%d and  exists ( select null from BOOKING d where  c.Plate=d.CarPlate and d.State=3)	and not exists  (select null from BOOKING b where   c.Plate=b.CarPlate and b.State = 2  and ( (  b.FromDate >= %d and b.FromDate <= %d ) or (  b.ToDate >= %d and b.ToDate <= %d )) ) ",
				category, from.getTime(), to.getTime(), from.getTime(), to.getTime());
		try (java.sql.Connection con = DbManager.getConnection(); Statement stmt = con.createStatement();) {

			ResultSet rs = stmt.executeQuery(sql);

			List<SaleCar> cars = new ArrayList<>();

			while (rs.next()) {
				Car car = new Car(new Date(rs.getLong("InServiceDate")), rs.getString("Brand"), rs.getInt("Category"),
						rs.getString("Color"), rs.getString("Plate"), rs.getInt("State"));
				car.setScore(rs.getInt("Score"));
				SaleCar saleCar = new upem.car.svc.sale.SaleCar(car, rs.getDouble("Price"));
				cars.add(saleCar);
			}

			return cars;
		}

	}
	/*
	 * public static SaleCar getACar(String plate) throws SQLException {
	 * 
	 * String sql = String.format(
	 * "select c.InServiceDate 'InServiceDate', c.Brand 'Brand', c.Category 'Category', c.Color 'Color', c.Plate 'Plate', c.State 'State', p.Price 'Price', c.Score 'Score'  from CAR c  inner join SALEPRICE p on p.CarPlate = c.Plate where lower(c.Plate)='%s' "
	 * , plate.toLowerCase()); try (java.sql.Connection con =
	 * DbManager.getConnection(); Statement stmt = con.createStatement();) {
	 * 
	 * ResultSet rs = stmt.executeQuery(sql);
	 * 
	 * if (rs.next()) { Car car = new Car(new Date(rs.getLong("InServiceDate")),
	 * rs.getString("Brand"), rs.getInt("Category"), rs.getString("Color"),
	 * rs.getString("Plate"), rs.getInt("State")); car.setScore(rs.getInt("Score"));
	 * SaleCar saleCar = new upem.car.svc.sale.SaleCar(car, rs.getDouble("Price"));
	 * return saleCar; } else throw new
	 * IllegalStateException("Error: car not existing" + plate);
	 * 
	 * }
	 * 
	 * }
	 */

	public static SaleCar getACar(String plate, java.sql.Connection con) throws SQLException {

		Date from = Util.getToday();
		Date to = Util.getMaxDate();

		String sql = String.format(
				"select c.InServiceDate 'InServiceDate', c.Brand 'Brand', c.Category 'Category', c.Color 'Color', c.Plate 'Plate', c.State 'State', p.Price 'Price' , c.Score 'Score' from CAR c  inner join SALEPRICE p on p.CarPlate = c.Plate  where c.State = 1 and upper(c.Plate)='%s'  and  exists ( select null from BOOKING d where  c.Plate=d.CarPlate and d.State=3)	and not exists  (select null from BOOKING b where   c.Plate=b.CarPlate and b.State = 2  and ( (  b.FromDate >= %d and b.FromDate <= %d ) or (  b.ToDate >= %d and b.ToDate <= %d )) ) ",
				plate.toUpperCase(), from.getTime(), to.getTime(), from.getTime(), to.getTime());
		try (Statement stmt = con.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
			SaleCar saleCar = null;
			if (!rs.next())
				throw new IllegalStateException("car not available:" + plate.toUpperCase() + " for period"
						+ from.toLocaleString() + " to " + to.toLocaleString());

			Car car = new Car(new Date(rs.getLong("InServiceDate")), rs.getString("Brand"), rs.getInt("Category"),
					rs.getString("Color"), rs.getString("Plate"), rs.getInt("State"));
			car.setScore(rs.getInt("Score"));
			saleCar = new upem.car.svc.sale.SaleCar(car, rs.getDouble("Price"));

			if (rs.next())
				throw new IllegalStateException("to much car:" + plate.toUpperCase());

			return saleCar;

		}

	}

}
