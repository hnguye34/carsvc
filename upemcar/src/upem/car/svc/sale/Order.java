package upem.car.svc.sale;

import java.io.Serializable;

import java.util.Date;


public class Order implements Serializable {

	private OrderItem[] cars;
	private String ccy;
	private double xRate;
	private String custId;
	private long orderId;
	private Date payDate;
	
	public Order(long orderId, String custId,  String ccy, double xRate,  Date payDate) {
		super();
		this.ccy = ccy;
		this.xRate = xRate;
		this.custId = custId;
		this.orderId = orderId;
		this.payDate = payDate;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long l) {
		this.orderId = l;
	}

	
	
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}


	
	
	public Order() {

	}

	public Order(String custId, String ccy) {
		this.setCustId(custId);
		this.ccy = ccy;
		
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setCars(OrderItem[] cars) {
		this.cars = cars;
	}

	//
	public OrderItem[] getCars() {
		// TODO Auto-generated method stub
		return cars;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public double getxRate() {
		return xRate;
	}

	public void setxRate(double xRate) {
		this.xRate = xRate;
	}

	public void addCar(OrderItem orderItem) {
		OrderItem[] newCars;
		if (this.cars == null)
		{
			this.cars = new OrderItem[1];
			this.cars[0] = orderItem;
		}
		else {
			newCars = new OrderItem[this.cars.length + 1];
			System.arraycopy(this.cars, 0, newCars, 0, this.cars.length);
			newCars[this.cars.length] = orderItem;
			this.cars = newCars;
		}
		

	}

	public void convertPrice(double xRate) {

		if (xRate == 1)
			return;

		for (OrderItem saleCar : cars) {
			saleCar.convert(xRate);
			;
		}

	}

}
