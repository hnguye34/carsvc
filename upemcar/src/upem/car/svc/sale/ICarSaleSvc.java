package upem.car.svc.sale;

import java.rmi.RemoteException;
import java.sql.SQLException;

import javax.xml.rpc.ServiceException;

/**
 * @author Nam Nguyen
 *
 */

public interface ICarSaleSvc {

	/**
	 * get cars available for sale
	 * 
	 * @param category
	 * @param placeNumber
	 * @param brand
	 * @param ccy
	 * @return
	 * @throws SQLException
	 * @throws ServiceException
	 * @throws RemoteException
	 */
	SaleCar[] getAvailableCarsForSale(int category, String brand, String ccy) throws Exception;

	/**
	 * Add cars in the shoping cart
	 * 
	 * @param custId
	 * @param passwd
	 * @param profil
	 * @param carPlate
	 * @param ccy
	 * @param carPrice
	 * @return the shopping cart after adding the new car
	 * @throws Exception
	 */
	ShoppingCart addInCart(String custId, String passwd, int profil, String carPlate, String ccy, double carPrice)
			throws Exception;

	/**
	 * Get the content of the shopping cart
	 * 
	 * @param carId
	 * @param custId
	 * @param passwd
	 * @param profil
	 * @return the cars list
	 */

	ShoppingCart getCart(String custId, String passwd,  int profil) throws Exception;

	/**
	 * buy
	 * 
	 * @param custId
	 * @param passwd
	 * @param profil
	 * @param cardId
	 * @param code
	 * @param expiredDate
	 * @return 
	 * @throws Exception
	 */
	Order buy(String custId, String passwd, int profil, String cardId, String code, String expiredDate,
			ShoppingCart cart) throws Exception;

	/**
	 * remove a car from the shopping cart
	 * 
	 * @param cartId
	 * @param custId
	 * @param passwd
	 * @param profil
	 * @return
	 */
	ShoppingCart removeFromCart(String custId, String passwd, int profil, String carPlate) throws Exception;

	/**
	 * get all past transaction
	 * 
	 * @param custId
	 * @param passwd
	 * @param profil
	 * @return
	 * @throws SQLException
	 */
	ShoppingCart[] getHistoTransaction(String custId, String passwd, int profil) throws Exception;

	/**
	 * change the currency of the shopping cart
	 * 
	 * @param custId
	 * @param passwd
	 * @param profil
	 * @param ccy
	 * @return
	 * @throws Exception
	 */
	ShoppingCart changeCcy(String custId, String passwd, int profil, String ccy) throws Exception;

}
