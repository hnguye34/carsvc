package upem.car.svc.sale;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import payment.PaySvc;
import payment.PaySvcServiceLocator;
import upem.car.authentification.CarAuthenficationSvc;

import upem.car.authentification.ICarAuthenficationSvc;
import upem.car.dao.CarDao;
import upem.car.dao.DbManager;
import upem.car.dao.OrderDao;
import upem.car.dao.SaleCarDao;
import upem.car.dao.ShoppingCartDao;
import upem.car.svc.common.Car;
import upem.car.util.Util;

public class CarSaleSvc implements ICarSaleSvc {

	public CarSaleSvc() throws RemoteException {

	}

	@Override
	public ShoppingCart addInCart(String custId, String passwd, int profil, String carPlate, String ccy,
			double carPrice) throws Exception {

		ICarAuthenficationSvc authSvc = new CarAuthenficationSvc();

		if (!authSvc.logIn(custId, passwd, profil))
			throw new IllegalStateException("Cannot sign in");

		try (java.sql.Connection con = DbManager.getConnection()) {
			boolean isAutoCommit = con.getAutoCommit();
			con.setAutoCommit(false);
			con.beginRequest();
			try {

				SaleCar saleCar = SaleCarDao.getACar(carPlate, con);
				if (Car.State.fromId(saleCar.getCar().getState()) == Car.State.SOLD)
					throw new IllegalStateException("Error: vehicule already sold:" + carPlate);

				double xRate = Util.getXRate(ccy);
				double price = xRate * saleCar.getPriceinEUR();

				if (price != carPrice)
					throw new IllegalStateException(
							"Error: price has been changed. Please update with the lastest price:" + carPlate + price);

				ShoppingCart cart = ShoppingCartDao.createIfAbsentShoppingCart(custId, con, ccy);
				if (cart.getCars() != null)
					for (SaleCar c : cart.getCars()) {
						if (c.getCar().getLicensePlate().equalsIgnoreCase(carPlate))
							throw new IllegalStateException("Car already in your shping cart:" + carPlate);

					}
				ShoppingCartDao.addCar(cart, carPlate, con);

				// reload the cart from db
				cart = ShoppingCartDao.getShoppingCart(custId, con);
				if (cart == null)
					throw new IllegalStateException("Erreur d'ajout de v�hicule");
				cart.convertPrice(xRate);
				cart.setxRate(xRate);

				con.commit();
				return cart;

			} catch (Exception e) {
				con.rollback();
				throw e;

			} finally {
				con.endRequest();
				con.setAutoCommit(isAutoCommit);
			}
		}

	}

	public SaleCar[] getAvailableCarsForSale(int category, String brand, String ccy) throws Exception, RemoteException {

		ccy = ccy.isEmpty() ? Util.CCY_BY_DEFAULT : ccy.toUpperCase();

		double xRate = Util.getXRate(ccy);

		List<SaleCar> cars = SaleCarDao.getAvailableCars(Util.getToday(), Util.getMaxDate(), category);

		// Conversion in good ccy
		
		  cars = cars.stream() .filter(car -> brand == null || brand.isEmpty() ||
		  car.getCar().getBrand().equalsIgnoreCase(brand))
		  .collect(Collectors.toList());
		  
		  cars.forEach(car -> { car.convert(xRate); });
		 
		// Apply conversion.
		return (SaleCar[]) cars.toArray(new SaleCar[cars.size()]);

	}

	@Override
	public ShoppingCart removeFromCart(String custId, String passwd, int profil, String carPlate) throws Exception {
		ICarAuthenficationSvc authSvc = new CarAuthenficationSvc();

		if (!authSvc.logIn(custId, passwd, profil))
			throw new IllegalStateException("Cannot sign in");

		try (java.sql.Connection con = DbManager.getConnection()) {
			boolean isAutoCommit = con.getAutoCommit();
			con.setAutoCommit(false);
			try {
				Car car = CarDao.getCar(carPlate, con);
				ShoppingCartDao.removeCar(con, custId, car);

				// reload the cart from db
				ShoppingCart cart = ShoppingCartDao.getShoppingCart(custId, con);
				if (cart == null)
					throw new IllegalStateException("Erreur de retirer le v�hicule:" + carPlate);

				double xRate = Util.getXRate(cart.getCcy());
				cart.convertPrice(xRate);
				cart.setxRate(xRate);

				con.commit();
				return cart;
			}

			catch (Exception e) {
				con.rollback();
				throw e;

			} finally {
				con.endRequest();
				con.setAutoCommit(isAutoCommit);
			}
		}
	}

	@Override
	public ShoppingCart changeCcy(String custId, String passwd, int profil, String ccy) throws Exception {

		ICarAuthenficationSvc authSvc = new CarAuthenficationSvc();

		if (!authSvc.logIn(custId, passwd, profil))
			throw new IllegalStateException("Cannot sign in");

		double xRate = Util.getXRate(ccy);

		try (java.sql.Connection con = DbManager.getConnection()) {
			boolean isAutoCommit = con.getAutoCommit();

			try {
				ShoppingCart cart = ShoppingCartDao.getShoppingCart(custId, con);
				if (cart == null)
					throw new IllegalStateException("panier inexistant pour ce client:" + custId);

				ShoppingCartDao.setCcy(cart, ccy, con);

				cart.convertPrice(xRate);
				cart.setxRate(xRate);

				con.commit();
				return cart;
			}

			catch (Exception e) {
				con.rollback();
				throw e;

			} finally {
				con.endRequest();
				con.setAutoCommit(isAutoCommit);
			}
		}
	}

	@Override
	public Order buy(String custId, String passwd, int profil, String cardId, String code, String expiredDate,
			ShoppingCart cart) throws Exception {

		ICarAuthenficationSvc authSvc = new CarAuthenficationSvc();

		if (!authSvc.logIn(custId, passwd, profil))
			throw new IllegalStateException("Cannot sign in");
		try (java.sql.Connection con = DbManager.getConnection()) {
			boolean isAutoCommit = con.getAutoCommit();
			con.setAutoCommit(false);
			con.beginRequest();
			try {

				
				
				double xRate = Util.getXRate(cart.getCcy());

				if (cart.getCars() == null)
					throw new IllegalStateException("panier vide");

				Objects.requireNonNull(cart.getCars());
				// check price
				double totalAmount = 0;
				for (SaleCar c : cart.getCars()) {

					if (Car.State.fromId(c.getCar().getState()) == Car.State.SOLD)
						throw new IllegalStateException("Error: vehicule already sold:" + c.getCar().getLicensePlate());

					double clientPrice = c.getPrice();
					SaleCar saleCar = SaleCarDao.getACar(c.getCar().getLicensePlate(), con); // also to lock the car
					if (saleCar == null)
						throw new IllegalStateException("vehicule inexistant:" + c.getCar().getLicensePlate());

					if (saleCar.getPriceinEUR() != c.getPriceinEUR() || saleCar.getPrice() != c.getPrice())
						throw new IllegalStateException(
								"Error: price has been changed. Please update with the lastest price:"
										+ saleCar.getCar().getLicensePlate());
					
					totalAmount += clientPrice;

				}

				Order order = OrderDao.createIfAbsentShoppingCart(con, custId, new Date(), cart.getCcy(), xRate);
				for (SaleCar c : cart.getCars()) {
					OrderDao.addCar(order, c.getCar().getLicensePlate(), c.getPrice(), con);
					CarDao.setState(c.getCar().getLicensePlate(), Car.State.SOLD, con);
					ShoppingCartDao.removeCar(con, custId, c.getCar());
					OrderItem orderItem = OrderDao.getOrderItem(order, c.getCar().getLicensePlate(),  con);
					order.addCar(orderItem);
				}

				PaySvc paySvc = new PaySvcServiceLocator().getPaySvc();
				
				if (!paySvc.pay(cardId, expiredDate, code, totalAmount, order.getCcy()))
								throw new IllegalStateException("La r�servation ne peut pas �tre effectu�e en raison du paiement impossible.");
				
				con.commit();
				
				return order;
			} catch (Exception e) {
				con.rollback();
				throw e;
			} finally {
				con.setAutoCommit(isAutoCommit);
				con.endRequest();
			}
		}

	}

	@Override
	public ShoppingCart getCart(String custId, String passwd, int profil) throws Exception {
		ICarAuthenficationSvc authSvc = new CarAuthenficationSvc();

		if (!authSvc.logIn(custId, passwd, profil))
			throw new IllegalStateException("Cannot sign in");
		try (java.sql.Connection con = DbManager.getConnection()) {
			boolean isAutoCommit = con.getAutoCommit();
			con.setAutoCommit(false);
			try {
				ShoppingCart cart = ShoppingCartDao.createIfAbsentShoppingCart(custId, con, "EUR");
				if (cart == null)
					throw new IllegalStateException("panier inexistant pour ce client:" + custId);

				con.commit();
				return cart;
			} catch (Exception e) {
				con.rollback();
			} finally {
				con.setAutoCommit(isAutoCommit);
			}
		}

		return null;
	}

	@Override
	public ShoppingCart[] getHistoTransaction(String custId, String passwd, int profil) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
