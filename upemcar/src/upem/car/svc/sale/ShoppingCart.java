package upem.car.svc.sale;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Date;

import upem.car.svc.common.Car.State;
import upem.car.util.Util;

public class ShoppingCart implements Serializable {

	public enum State {  CREATED (1), PAID(2) ;
		
		private int id;
		State(int id) { this.id = id ; }
		
		 public static State fromId(int id) {
                for (State type : values()) {
                    if (type.id == id) {
                        return type;
                    }
                }
                throw new IllegalArgumentException();
            }
		 
	} 

	private SaleCar[] cars;
	private String ccy;
	private double xRate;
	private String custId;
	private int cartId;
	private State state;
	
	
	public ShoppingCart() {

	}

	public ShoppingCart(String custId, String ccy) {
		this.setCustId(custId);
		this.ccy = ccy;
		
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setCars(SaleCar[] cars) {
		this.cars = cars;
	}

	//
	public SaleCar[] getCars() {
		// TODO Auto-generated method stub
		return cars;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public double getxRate() {
		return xRate;
	}

	public void setxRate(double xRate) {
		this.xRate = xRate;
	}

	public void addCar(SaleCar saleCar) {
		SaleCar[] newCars;
		if (this.cars == null)
		{
			this.cars = new SaleCar[1];
			this.cars[0] = saleCar;
		}
		else {
			newCars = new SaleCar[this.cars.length + 1];
			System.arraycopy(this.cars, 0, newCars, 0, this.cars.length);
			newCars[this.cars.length] = saleCar;
			this.cars = newCars;
		}
		

	}

	public void convertPrice(double xRate) {

		if (xRate == 1)
			return;

		for (SaleCar saleCar : cars) {
			saleCar.convert(xRate);
			;
		}

	}

}
