package upem.car.svc.sale;

import java.io.Serializable;

import upem.car.svc.common.Car;

public class SaleCar implements Serializable {
	
	private Car car;
	private double price;
	
	public SaleCar() {
		
	}

	public SaleCar(Car car,  double priceinEUR) {
		this.car = car;
		this.price = priceinEUR;
		this.priceinEUR = priceinEUR;
	}
	
	public Car getCar() {
		return car;
	}

	public void convert (double xRate) {
		if ( xRate == 0 )
			throw new IllegalArgumentException(String.valueOf(xRate));
		
		this.price = xRate * this.priceinEUR ;
	}
	public void setCar(Car car) {
		this.car = car;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double priceinEUR;
	
	public double getPriceinEUR() {
		return priceinEUR;
	}

	public void setPriceinEUR(double priceinEUR) {
		this.priceinEUR = priceinEUR;
	}

	
	
	
};