package upem.car.svc.common;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


public class Car implements Serializable{

	public enum Category {  SEDAN (1), MONOSPACE(2) ;
		
		private int id;
		Category(int id) { this.id = id ; }
		
		 public static Category fromId(int id) {
                for (Category type : values()) {
                    if (type.id == id) {
                        return type;
                    }
                }
                throw new IllegalArgumentException();
            }
		 
	} 
	public enum State {  IN_SERVICE (1), SOLD(2) ;
		
		private int id;
		State(int id) { this.id = id ; }
		
		 public static State fromId(int id) {
                for (State type : values()) {
                    if (type.id == id) {
                        return type;
                    }
                }
                throw new IllegalArgumentException();
            }
		 public int id (){
			 return id;
		 }
		 
	} 
	private Date inServiceDate;
	private String brand;
	private int category;
	private String color;
	private String licensePlate;
	private int state;
	private boolean alreadyRented;
	public boolean isAlreadyRented() {
		return alreadyRented;
	}

	public void setAlreadyRented(boolean alreadyRented) {
		this.alreadyRented = alreadyRented;
	}

	private int score;
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		Objects.nonNull(State.fromId(state));
		this.state = state;
	}

	public Car() {
		
	}
	
	public Car(Date inServiceDate, String brand, int category, String color, String licensePlate, int state) {
		super();
		this.inServiceDate = inServiceDate;
		this.brand = brand;
		Objects.requireNonNull(Category.fromId(category));
		this.category = category;
		this.color = color;
		this.licensePlate = licensePlate;
		this.state = state;
		this.alreadyRented = false;
	}

	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public Date getInServiceDate() {
		return inServiceDate;
	}
	public void setInServiceDate(Date inServiceDate) {
		this.inServiceDate = inServiceDate;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		Objects.requireNonNull(Category.fromId(category));
		this.category = category;
			
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}


	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	
}
