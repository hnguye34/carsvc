package upem.corps.authentification.svc;

import java.rmi.*;
import java.sql.SQLException;

public interface IUserManager  extends Remote {
	int port = 1098;
	String adress = "UpemAuthentificationSvc";
	UpemUser getUser (String id, String passwd )  throws RemoteException;
	boolean checkUser(String id, String passwd )  throws RemoteException, SQLException;
};
