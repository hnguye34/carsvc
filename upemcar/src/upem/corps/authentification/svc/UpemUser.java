package upem.corps.authentification.svc;

import java.io.Serializable;
public class UpemUser  implements Serializable {
	
	public UpemUser(String id, String email, String firstName, String lastName, String passwd) {
	
		this.id = id;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.passwd = passwd;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	private String getPasswd() {
		return passwd;
	}
	private void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	private String id;
	private String email;
	private String firstName;
	private String lastName;
	private String passwd;
	

}
