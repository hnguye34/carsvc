package payment;

public class PaySvcProxy implements payment.PaySvc {
  private String _endpoint = null;
  private payment.PaySvc paySvc = null;
  
  public PaySvcProxy() {
    _initPaySvcProxy();
  }
  
  public PaySvcProxy(String endpoint) {
    _endpoint = endpoint;
    _initPaySvcProxy();
  }
  
  private void _initPaySvcProxy() {
    try {
      paySvc = (new payment.PaySvcServiceLocator()).getPaySvc();
      if (paySvc != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)paySvc)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)paySvc)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (paySvc != null)
      ((javax.xml.rpc.Stub)paySvc)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public payment.PaySvc getPaySvc() {
    if (paySvc == null)
      _initPaySvcProxy();
    return paySvc;
  }
  
  public boolean pay(java.lang.String carId, java.lang.String expiredDate, java.lang.String code, double amount, java.lang.String ccy) throws java.rmi.RemoteException{
    if (paySvc == null)
      _initPaySvcProxy();
    return paySvc.pay(carId, expiredDate, code, amount, ccy);
  }
  
  
}