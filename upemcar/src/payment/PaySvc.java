/**
 * PaySvc.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package payment;

public interface PaySvc extends java.rmi.Remote {
    public boolean pay(java.lang.String cardId, java.lang.String expiredDate, java.lang.String code, double amount, java.lang.String ccy) throws java.rmi.RemoteException;
}
