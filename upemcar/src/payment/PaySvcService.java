/**
 * PaySvcService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package payment;

public interface PaySvcService extends javax.xml.rpc.Service {
    public java.lang.String getPaySvcAddress();

    public payment.PaySvc getPaySvc() throws javax.xml.rpc.ServiceException;

    public payment.PaySvc getPaySvc(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
