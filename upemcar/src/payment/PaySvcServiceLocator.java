/**
 * PaySvcServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package payment;

public class PaySvcServiceLocator extends org.apache.axis.client.Service implements payment.PaySvcService {

    public PaySvcServiceLocator() {
    }


    public PaySvcServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PaySvcServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PaySvc
    private java.lang.String PaySvc_address = "http://localhost:8080/bank/services/PaySvc";

    public java.lang.String getPaySvcAddress() {
        return PaySvc_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PaySvcWSDDServiceName = "PaySvc";

    public java.lang.String getPaySvcWSDDServiceName() {
        return PaySvcWSDDServiceName;
    }

    public void setPaySvcWSDDServiceName(java.lang.String name) {
        PaySvcWSDDServiceName = name;
    }

    public payment.PaySvc getPaySvc() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PaySvc_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPaySvc(endpoint);
    }

    public payment.PaySvc getPaySvc(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            payment.PaySvcSoapBindingStub _stub = new payment.PaySvcSoapBindingStub(portAddress, this);
            _stub.setPortName(getPaySvcWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPaySvcEndpointAddress(java.lang.String address) {
        PaySvc_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (payment.PaySvc.class.isAssignableFrom(serviceEndpointInterface)) {
                payment.PaySvcSoapBindingStub _stub = new payment.PaySvcSoapBindingStub(new java.net.URL(PaySvc_address), this);
                _stub.setPortName(getPaySvcWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PaySvc".equals(inputPortName)) {
            return getPaySvc();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://payment", "PaySvcService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://payment", "PaySvc"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PaySvc".equals(portName)) {
            setPaySvcEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
