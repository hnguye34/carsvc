package payment;

public interface IPaySvc {
	boolean pay(String carId, String expiredDate, String code, double amount, String ccy );
}
