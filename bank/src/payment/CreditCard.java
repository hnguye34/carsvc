package payment;

import java.io.Serializable;

public class CreditCard implements Serializable{

	String carId;
	public CreditCard(String carId, String expiredDate, String code, double amount, String ccy) {
		super();
		this.carId = carId;
		this.expiredDate = expiredDate;
		this.code = code;
		this.amount = amount;
		this.ccy = ccy;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	String expiredDate;
	String code;
	double amount;
	String ccy;
	
}
