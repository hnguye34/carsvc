package payment;


import java.rmi.RemoteException;
import java.util.concurrent.ConcurrentHashMap;



public class PaySvc implements IPaySvc{
	
	ConcurrentHashMap<String, CreditCard> cars = new ConcurrentHashMap<>();
	
	public PaySvc() throws  RemoteException {
		cars.put( "12345" , new CreditCard ("12345", "0101", "ABC", 40000, "EUR") );
		cars.put( "6489" , new CreditCard ("12345", "0101", "ABC", 10000, "EUR") );
	}
	
	@Override
	public boolean pay(String cardId, String expiredDate, String code, double amount, String ccy) {

		
		CreditCard cb = cars.get(cardId) ;
		if ( cb == null)
			throw new  IllegalStateException ("cardId incorrect:" + cardId);
		
		if ( ! cb.getExpiredDate().equalsIgnoreCase(expiredDate))
			throw new  IllegalStateException ("expiredDate incorrect:" + expiredDate);
		
		if ( ! cb.getCode().equalsIgnoreCase(code) )
			throw new  IllegalStateException ("code incorrect:" + code);
		
		if ( ! cb.getCcy().equalsIgnoreCase(ccy) )
			throw new  IllegalStateException ("ccy incorrect:" + ccy);
		
		if (  cb.getAmount() < amount )
			throw new  IllegalStateException ("Montant d�passe:" + String.valueOf(cb.getAmount()));
		
		return true;
		
	}

}
